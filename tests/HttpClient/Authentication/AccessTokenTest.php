<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Test\HttpClient\Authentication;

use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Wagento\GunBrokerApi\HttpClient\Authentication\AccessToken;

final class AccessTokenTest extends TestCase
{
    public function testConfiguresRequestAuthenticationHeader(): void
    {
        $accessToken = new AccessToken('7eb44c71fa2b412c89f9a91833ba1212');
        $requestMock = $this->createMock(RequestInterface::class);

        $requestMock->expects(self::once())
            ->method('withHeader')
            ->with('X-AccessToken', '7eb44c71fa2b412c89f9a91833ba1212')
            ->willReturnSelf();

        $accessToken->authenticate($requestMock);
    }
}
