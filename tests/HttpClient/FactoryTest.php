<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Test\HttpClient;

use Http\Client\Common\Plugin;
use Http\Mock\Client;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Wagento\GunBrokerApi\HttpClient\Factory;
use Wagento\GunBrokerApi\HttpClient\Plugins\ExceptionPlugin;

use function get_class;

final class FactoryTest extends TestCase
{
    private Factory $factory;

    public function testDefaultPluginsAreAdded(): void
    {
        self::assertTrue($this->factory->hasPlugin(Plugin\HeaderDefaultsPlugin::class));
        self::assertTrue($this->factory->hasPlugin(ExceptionPlugin::class));
    }

    public function testCanAddOnePlugin(): void
    {
        /** @var Plugin|MockObject $plugin */
        $plugin = $this->getMockBuilder(Plugin::class)->getMock();

        $this->factory->addPlugin($plugin);

        self::assertTrue($this->factory->hasPlugin(get_class($plugin)));
    }

    public function testCanAddMultiplePlugins(): void
    {
        $plugins = [
            $this->getMockBuilder(Plugin::class)->getMock(),
            $this->getMockBuilder(Plugin::class)->getMock()
        ];

        $this->factory->addPlugins($plugins);

        self::assertTrue($this->factory->hasPlugin(get_class($plugins[0])));
        self::assertTrue($this->factory->hasPlugin(get_class($plugins[1])));
    }

    public function testCanRemoveOnePlugin(): void
    {
        /** @var Plugin|MockObject $plugin */
        $plugin = $this->getMockBuilder(Plugin::class)->getMock();

        $this->factory->addPlugin($plugin);
        $this->factory->removePlugin(get_class($plugin));

        self::assertFalse($this->factory->hasPlugin(get_class($plugin)));
    }

    public function testCanRemoveMultiplePlugins(): void
    {
        $plugins = [
            $this->getMockBuilder(Plugin::class)->getMock(),
            $this->getMockBuilder(Plugin::class)->getMock()
        ];

        $this->factory->addPlugins($plugins);
        $this->factory->removePlugins([
            get_class($plugins[0]),
            get_class($plugins[1]),
        ]);

        self::assertFalse($this->factory->hasPlugin(get_class($plugins[0])));
        self::assertFalse($this->factory->hasPlugin(get_class($plugins[1])));
    }

    public function testCanRemoveAllPlugins(): void
    {
        /** @var Plugin|MockObject $plugin */
        $plugin = $this->getMockBuilder(Plugin::class)->getMock();

        $this->factory->addPlugin($plugin);
        $this->factory->resetPlugins();

        self::assertEmpty($this->factory->getPlugins());
    }

    public function testCanAddOneHttpHeader(): void
    {
        $this->factory->addHttpHeader('X-Foo', 'Bar');

        self::assertTrue($this->factory->hasHttpHeader('X-Foo'));
    }

    public function testCanAddMultipleHttpHeaders(): void
    {
        $this->factory->addHttpHeaders([
            'X-Alpha' => 'Beta',
            'X-Gamma' => 'Delta'
        ]);

        self::assertTrue($this->factory->hasHttpHeader('X-Alpha'));
        self::assertTrue($this->factory->hasHttpHeader('X-Gamma'));
    }

    public function testCanRemoveOneHttpHeader(): void
    {
        $this->factory->addHttpHeader('X-Foo', 'Bar');
        $this->factory->removeHttpHeader('X-Foo');

        self::assertFalse($this->factory->hasHttpHeader('X-Foo'));
    }

    public function testCanRemoveMultipleHttpHeaders(): void
    {
        $this->factory->addHttpHeaders([
            'X-Alpha' => 'Beta',
            'X-Gamma' => 'Delta'
        ]);
        $this->factory->removeHttpHeaders([
            'X-Alpha',
            'X-Gamma'
        ]);

        self::assertFalse($this->factory->hasHttpHeader('X-Alpha'));
        self::assertFalse($this->factory->hasHttpHeader('X-Gamma'));
    }

    public function testCanRemoveAllHttpHeaders(): void
    {
        $this->factory->addHttpHeader('X-Foo', 'Bar');
        $this->factory->resetHttpHeaders();

        self::assertEmpty($this->factory->getHttpHeaders());
    }

    public function testCreatesConfiguredHttpClient(): void
    {
        self::markTestIncomplete(
            'TODO: Figure out how to test whether HttpMethodsClient is fully instantiated by Factory::create().'
        );
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->factory = new Factory(new Client());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->factory);
    }
}
