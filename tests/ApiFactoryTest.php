<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Test;

use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use ReflectionClass;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiFactory;
use PHPUnit\Framework\TestCase;
use Wagento\GunBrokerApi\Exception\InvalidApiEndpointException;

final class ApiFactoryTest extends TestCase
{
    public function testInstantiatesApiRequestClassWithoutConstructorArguments(): void
    {
        $apiFactory = new ApiFactory();
        $api = $apiFactory->create('GunBrokerTime');

        self::assertInstanceOf(Api::class, $api);
    }

    public function testInstantiatesApiRequestClassWithConstructorArguments(): void
    {
        $streamFactoryMock = $this->createMock(StreamFactoryInterface::class);
        $requestFactoryMock = $this->createMock(RequestFactoryInterface::class);
        $apiFactory = new ApiFactory($streamFactoryMock, $requestFactoryMock);
        $api = $apiFactory->create('Items');
        $reflectionClass = new ReflectionClass($api);
        $streamFactoryProperty = $reflectionClass->getProperty('streamFactory');
        $requestFactoryProperty = $reflectionClass->getProperty('requestFactory');

        $streamFactoryProperty->setAccessible(true);
        $requestFactoryProperty->setAccessible(true);

        self::assertInstanceOf(StreamFactoryInterface::class, $streamFactoryProperty->getValue($api));
        self::assertInstanceOf(RequestFactoryInterface::class, $requestFactoryProperty->getValue($api));
        self::assertInstanceOf(Api::class, $api);
    }

    public function testThrowsExceptionForInvalidApiClass(): void
    {
        $this->expectException(InvalidApiEndpointException::class);

        $apiFactory = new ApiFactory();

        $apiFactory->create('Foo');
    }
}
