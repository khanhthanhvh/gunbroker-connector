<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Test;

use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Exception;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Message\RequestMatcher;
use Http\Mock\Client;
use Http\Promise\Promise;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;
use RuntimeException;
use TypeError;

use function get_debug_type;
use function is_string;
use function sprintf;

final class MockHttpMethodsClient implements HttpMethodsClientInterface
{
    private ClientInterface $httpClient;
    private RequestFactoryInterface $requestFactory;
    private StreamFactoryInterface $streamFactory;

    public function __construct(
        ?ClientInterface $httpClient = null,
        ?RequestFactoryInterface $requestFactory = null,
        ?StreamFactoryInterface $streamFactory = null
    ) {
        $this->httpClient = $httpClient ?? new Client();
        $this->requestFactory = $requestFactory ?? Psr17FactoryDiscovery::findRequestFactory();
        $this->streamFactory = $streamFactory ?? Psr17FactoryDiscovery::findStreamFactory();

        if (!$this->httpClient instanceof Client) {
            throw new TypeError('Argument $httpClient should be an instance of ' . Client::class . '.');
        }
    }

    /**
     * @inheritDoc
     * @param array<string, string> $headers
     */
    public function get($uri, array $headers = []): ResponseInterface
    {
        return $this->send('GET', $uri, $headers, null);
    }

    /**
     * @inheritDoc
     * @param array<string, string> $headers
     */
    public function head($uri, array $headers = []): ResponseInterface
    {
        return $this->send('HEAD', $uri, $headers, null);
    }

    /**
     * @inheritDoc
     * @param array<string, string> $headers
     */
    public function trace($uri, array $headers = []): ResponseInterface
    {
        return $this->send('TRACE', $uri, $headers, null);
    }

    /**
     * @inheritDoc
     * @param array<string, string> $headers
     */
    public function post($uri, array $headers = [], $body = null): ResponseInterface
    {
        return $this->send('POST', $uri, $headers, $body);
    }

    /**
     * @inheritDoc
     * @param array<string, string> $headers
     */
    public function put($uri, array $headers = [], $body = null): ResponseInterface
    {
        return $this->send('PUT', $uri, $headers, $body);
    }

    /**
     * @inheritDoc
     * @param array<string, string> $headers
     */
    public function patch($uri, array $headers = [], $body = null): ResponseInterface
    {
        return $this->send('PATCH', $uri, $headers, $body);
    }

    /**
     * @inheritDoc
     * @param array<string, string> $headers
     */
    public function delete($uri, array $headers = [], $body = null): ResponseInterface
    {
        return $this->send('DELETE', $uri, $headers, $body);
    }

    /**
     * @inheritDoc
     * @param array<string, string> $headers
     */
    public function options($uri, array $headers = [], $body = null): ResponseInterface
    {
        return $this->send('OPTIONS', $uri, $headers, $body);
    }

    /**
     * @inheritDoc
     * @param array<string, string> $headers
     */
    public function send(string $method, $uri, array $headers = [], $body = null): ResponseInterface
    {
        if (!is_string($uri) && !$uri instanceof UriInterface) {
            throw new TypeError(
                sprintf(
                    '%s::send(): Argument #2 ($uri) must be of type string|%s, %s given',
                    self::class,
                    UriInterface::class,
                    get_debug_type($uri)
                )
            );
        }

        if (!is_string($body) && !$body instanceof StreamInterface && $body !== null) {
            throw new TypeError(
                sprintf(
                    '%s::send(): Argument #4 ($body) must be of type string|%s|null, %s given',
                    self::class,
                    StreamInterface::class,
                    get_debug_type($body)
                )
            );
        }

        return $this->sendRequest(
            $this->createRequest($method, $uri, $headers, $body)
        );
    }

    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        return $this->httpClient->sendRequest($request);
    }

    public function doSendRequest(RequestInterface $request): ResponseInterface
    {
        return $this->httpClient->doSendRequest($request);
    }

    /**
     * Adds an exception to be thrown or response to be returned if the request
     * matcher matches.
     *
     * For more complex logic, pass a callable as $result. The method is given
     * the request and MUST either return a ResponseInterface or throw an
     * exception that implements the PSR-18 / HTTPlug exception interface.
     *
     * @param ResponseInterface|Exception|ClientExceptionInterface|callable $result
     */
    public function on(RequestMatcher $requestMatcher, $result): void
    {
        $this->httpClient->on($requestMatcher, $result);
    }

    /**
     * Adds an exception that will be thrown.
     */
    public function addException(\Exception $exception): void
    {
        $this->httpClient->addException($exception);
    }

    /**
     * Sets the default exception to throw when the list of added exceptions and responses is exhausted.
     *
     * If both a default exception and a default response are set, the exception will be thrown.
     */
    public function setDefaultException(\Exception $defaultException = null): void
    {
        $this->httpClient->setDefaultException($defaultException);
    }

    /**
     * Adds a response that will be returned in first in first out order.
     */
    public function addResponse(ResponseInterface $response): void
    {
        $this->httpClient->addResponse($response);
    }

    /**
     * Sets the default response to be returned when the list of added exceptions and responses is exhausted.
     */
    public function setDefaultResponse(ResponseInterface $defaultResponse = null): void
    {
        $this->httpClient->addResponse($defaultResponse);
    }

    /**
     * Returns requests that were sent.
     *
     * @return RequestInterface[]
     */
    public function getRequests(): array
    {
        return $this->httpClient->getRequests();
    }

    public function getLastRequest(): RequestInterface
    {
        return $this->httpClient->getLastRequest();
    }

    public function reset(): void
    {
        $this->httpClient->reset();
    }

    public function sendAsyncRequest(RequestInterface $request): Promise
    {
        return $this->httpClient->sendAsyncRequest($request);
    }

    /**
     * @param string|UriInterface $uri
     * @param array<string, string> $headers
     * @param string|StreamInterface|null $body
     */
    private function createRequest(string $method, $uri, array $headers = [], $body = null): RequestInterface
    {
        if (is_string($body) && $body !== '' && $this->streamFactory === null) {
            throw new RuntimeException(
                'Cannot create request: A stream factory is required to create a request with a non-empty string body.'
            );
        }

        $request = $this->requestFactory->createRequest($method, $uri);

        foreach ($headers as $key => $value) {
            $request = $request->withHeader($key, $value);
        }

        if ($body !== null && $body !== '') {
            $request = $request->withBody(
                is_string($body) ? $this->streamFactory->createStream($body) : $body
            );
        }

        return $request;
    }
}
