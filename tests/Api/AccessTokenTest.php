<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Test\Api;

use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Wagento\GunBrokerApi\Api\Users\AccessToken;
use Wagento\GunBrokerApi\ApiObjects\Input\Users\AccessToken as AccessTokenRequest;
use Wagento\GunBrokerApi\Test\MockHttpMethodsClient;

final class AccessTokenTest extends TestCase
{
    public function testPostRetrievesApiTokenSuccessfully(): void
    {
        $accessTokenRequestData = new AccessTokenRequest();

        $accessTokenRequestData->username = 'user';
        $accessTokenRequestData->password = 'password';

        $accessTokenApi = new AccessToken();
        $accessTokenApi->httpClient = new MockHttpMethodsClient();
        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockStream = $this->createMock(StreamInterface::class);

        $mockResponse->method('getBody')->willReturn($mockStream);
        $mockResponse->method('getHeaderLine')->with('Content-Type')->willReturn('application/json');

        $mockStream->method('__toString')->willReturn(
            '{"accessToken": "d0c7c995be2f49efa398b083fcabd6cc"}'
        );

        $accessTokenApi->httpClient->addResponse($mockResponse);

        $response = $accessTokenApi->post($accessTokenRequestData);

        self::assertSame('d0c7c995be2f49efa398b083fcabd6cc', $response->accessToken);
    }

    public function testDeleteRemovesApiTokenSuccessfully(): void
    {
        $accessTokenApi = new AccessToken();
        $accessTokenApi->httpClient = new MockHttpMethodsClient();
        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockStream = $this->createMock(StreamInterface::class);

        $mockResponse->method('getBody')->willReturn($mockStream);
        $mockResponse->method('getHeaderLine')->with('Content-Type')->willReturn('application/json');

        $mockStream->method('__toString')->willReturn(
            '{"userMessage": "Access token deleted successfully.","developerMessage": "Access token deleted ' .
            'successfully.", "gbStatusCode": 0}'
        );

        $accessTokenApi->httpClient->addResponse($mockResponse);

        self::assertTrue($accessTokenApi->delete());
    }
}
