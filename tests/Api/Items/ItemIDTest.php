<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Test\Api\Items;

use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Wagento\GunBrokerApi\Api\Items\ItemID as ItemsApi;
use Wagento\GunBrokerApi\ApiObjects\Input\UpdatedItem as ItemUpdateRequestData;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Item as ItemResponseData;
use Wagento\GunBrokerApi\Test\MockHttpMethodsClient;

use function file_get_contents;

final class ItemIDTest extends TestCase
{
    public function testGetRetrievesItemSuccessfully(): void
    {
        $itemsApi = new ItemsApi();
        $itemsApi->httpClient = new MockHttpMethodsClient();
        $itemsApi->itemID = 11404334;

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockStream = $this->createMock(StreamInterface::class);

        $mockResponse->method('getBody')->willReturn($mockStream);
        $mockResponse->method('getHeaderLine')->with('Content-Type')->willReturn('application/json');

        $mockStream->method('__toString')->willReturn(file_get_contents(__DIR__ . '/../../_data/item.json'));

        $itemsApi->httpClient->addResponse($mockResponse);

        $response = $itemsApi->get();

        self::assertInstanceOf(ItemResponseData::class, $response);
    }

    public function testPutUpdatesItemSuccessfully(): void
    {
        $itemUpdateRequestData = new ItemUpdateRequestData();
        $itemUpdateRequestData->buyNowPrice = 5000.00;
        $itemUpdateRequestData->canOffer = true;
        $itemUpdateRequestData->cancelOpenOffers = false;
        $itemUpdateRequestData->standardTextID = 44;

        $itemsApi = new ItemsApi();
        $itemsApi->httpClient = new MockHttpMethodsClient();
        $itemsApi->itemID = 5;

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockStream = $this->createMock(StreamInterface::class);

        $mockResponse->method('getBody')->willReturn($mockStream);
        $mockResponse->method('getHeaderLine')->with('Content-Type')->willReturn('application/json');

        $mockStream->method('__toString')->willReturn(
            '{"userMessage": "Item updated","developerMessage": "Item updated", "gbStatusCode": 0, "links": []}'
        );

        $itemsApi->httpClient->addResponse($mockResponse);

        $response = $itemsApi->put($itemUpdateRequestData);

        self::assertInstanceOf(MessageResponse::class, $response);
    }
}
