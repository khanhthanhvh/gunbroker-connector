<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Test\Traits;

use PHPUnit\Framework\TestCase;
use Wagento\GunBrokerApi\HttpClient\Message\RequestProcessorInterface;
use Wagento\GunBrokerApi\Traits\ProcessesApiRequest;

use function get_class;

final class ProcessesApiRequestTest extends TestCase
{
    public function testWithRequestProcessor(): void
    {
        $mockRequestProcessor = $this->createMock(RequestProcessorInterface::class);
        $apiFake = new class {
            use ProcessesApiRequest;

            public string $foo;
            public string $bar;

            public function __construct(string $foo = '', string $bar = '')
            {
                $this->foo = $foo;
                $this->bar = $bar;
            }
        };

        $newApiFake = $apiFake->withRequestProcessor($mockRequestProcessor, 'foo', 'bar');

        self::assertInstanceOf(get_class($apiFake), $newApiFake);
        self::assertSame('foo', $newApiFake->foo);
    }
}
