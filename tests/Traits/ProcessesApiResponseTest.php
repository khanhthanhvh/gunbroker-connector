<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Test\Traits;

use PHPUnit\Framework\TestCase;
use Wagento\GunBrokerApi\HttpClient\Message\ResponseProcessorInterface;
use Wagento\GunBrokerApi\Traits\ProcessesApiResponse;

use function get_class;

final class ProcessesApiResponseTest extends TestCase
{
    public function testWithResponseProcessor(): void
    {
        $mockResponseProcessor = $this->createMock(ResponseProcessorInterface::class);
        $apiFake = new class {
            use ProcessesApiResponse;

            public string $foo;
            public string $bar;

            public function __construct(string $foo = '', string $bar = '')
            {
                $this->foo = $foo;
                $this->bar = $bar;
            }
        };

        $newApiFake = $apiFake->withResponseProcessor($mockResponseProcessor, 'foo', 'bar');

        self::assertInstanceOf(get_class($apiFake), $newApiFake);
        self::assertSame('foo', $newApiFake->foo);
    }
}
