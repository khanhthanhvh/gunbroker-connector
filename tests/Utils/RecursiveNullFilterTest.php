<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Test\Utils;

use Wagento\GunBrokerApi\ApiObjects\Input\Item\PremiumFeatures;
use Wagento\GunBrokerApi\Utils\RecursiveNullFilter;
use PHPUnit\Framework\TestCase;

use function array_filter;

final class RecursiveNullFilterTest extends TestCase
{
    public function testFiltersNullValuesWhenCalledStatically(): void
    {
        $data = [
            'integer' => 0,
            'float' => 1.5,
            'boolean' => false,
            'string' => 'foo',
            'null' => null,
            'array' => [
                'integer' => 0,
                'float' => 1.5,
                'boolean' => false,
                'string' => 'foo',
                'null' => null,
                'array' => [
                    'integer' => 0,
                    'float' => 1.5,
                    'boolean' => false,
                    'string' => 'foo',
                    'null' => null,
                    'data_object' => new PremiumFeatures()
                ],
                'data_object' => new PremiumFeatures()
            ],
            'data_object' => new PremiumFeatures()
        ];
        $expected = $data;
        /**
         * @param mixed $value
         */
        $filterFunction = static fn($value): bool => $value !== null;
        $expected['data_object'] = array_filter($expected['data_object']->all(), $filterFunction);
        $expected['array']['data_object'] = array_filter($expected['array']['data_object']->all(), $filterFunction);
        $expected['array']['array']['data_object'] = array_filter(
            $expected['array']['array']['data_object']->all(),
            $filterFunction
        );

        unset($expected['null'], $expected['array']['null'], $expected['array']['array']['null']);

        $actual = RecursiveNullFilter::filter($data);

        self::assertSame($expected, $actual);
   }
}
