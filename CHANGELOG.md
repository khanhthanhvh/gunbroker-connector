# Changelog for Wagento GunBroker API Connector

All notable changes to this library will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this library adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

For more information about the library, please refer to the
[README](./README.md) document.

## Unreleased

## [1.0.0] - 2021-12-13
### Added
- Initial release of library

[Unreleased]: https://bitbucket.org/wagento-global/gunbroker-connector/branches/compare/1.0.0...HEAD
[1.0.0]: https://bitbucket.org/wagento-global/gunbroker-connector/src/1.0.0