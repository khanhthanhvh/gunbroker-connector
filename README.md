# Wagento GunBroker API Connector

Wagento GunBroker API Connector is a PHP library made for interacting
with the [GunBroker.com](https://www.gunbroker.com/) firearm auction
service. This object-oriented API wrapper supports all functionality
provided by the GunBroker.com REST API, including managing item
listings, orders and more. Please see the
[official API documentation](https://api.gunbroker.com/User/) for more
details.

## Requirements

- PHP 7.4+ or 8.0+
    - [JSON extension required](https://www.php.net/json)
    - [UUID extension](https://pecl.php.net/package/uuid) recommended
- Composer 1.10+ or 2.0+
- [PSR-7](https://www.php-fig.org/psr/psr-7/) compliant HTTP Client
 (i.e. [Guzzle 7](https://github.com/guzzle/guzzle))
    - [HTTPlug adapter](http://docs.php-http.org/en/latest/clients.html)
      for client library (i.e. `php-http/guzzle7-adapter`)

<small>**Note**: This library uses
[HTTPlug](http://docs.php-http.org/en/latest/httplug/introduction.html)
to provide compatibility with PSR-7 complaint HTTP Client libraries.
Please see the
[HTTPlug Library User documentation](http://docs.php-http.org/en/latest/httplug/users.html)
for more details.</small>

## Installation

To install this library, please run the following command in your
terminal in the project that you wish to use it in:

    composer require wagento/gunbroker-connector

## Updating

To update this library, please run the following command in your
terminal:

    composer update wagento/gunbroker-connector

## Usage

The easiest way to use the GunBroker.com API is by instantiating
the convenience object located in `src/Connector.php`:

```php
<?php

declare(strict_types=1);

use Wagento\GunBrokerApi\ApiObjects\Input\Items;
use Wagento\GunBrokerApi\Connector;

require __DIR__ . '/vendor/autoload.php';

// Instantiate the connector
$gunBrokerConnector = new Connector();

// Configure the API settings:
$gunBrokerConnector->setApiEnvironment('sandbox'); // or "production"
$gunBrokerConnector->setApiDevKey('35b85bf5-95fb-4c21-b9ff-41bf4851bb17');
$gunBrokerConnector->configureAuthentication('api_user', 'api_password');

// Make some API calls:
$item = $gunBrokerConnector->getListedItem(12345);
print_r($item->all());

$itemsRequestData = new Items();
$itemsRequestData->itemIds = '12345,67890';
$items = $gunBrokerConnector->getListedItems($itemsRequestData);
print_r($items);
```

Alternatively, you can call the API handlers directly, making sure to
pass an instance of `\Http\Client\Common\HttpMethodsClientInterface`.

**This method is not recommended.**

```php
<?php

declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

use Http\Client\Common\Plugin\BaseUriPlugin;
use Http\Discovery\Psr17FactoryDiscovery;
use Wagento\GunBrokerApi\Api\Users\AccessToken as AccessTokenApi;
use Wagento\GunBrokerApi\ApiObjects\Input\Users\AccessToken;
use Wagento\GunBrokerApi\HttpClient\Factory as HttpClientFactory;

$httpClientFactory = new HttpClientFactory();
$httpClientFactory->addHttpHeader('X-DevKey', '35b85bf5-95fb-4c21-b9ff-41bf4851bb17');
$httpClientFactory->addPlugin(
    new BaseUriPlugin(
        Psr17FactoryDiscovery::findUriFactory()->createUri('https://api.gunbroker.com/v1/')
    )
);

$accessTokenRequestData = new AccessToken();
$accessTokenRequestData->username = 'user';
$accessTokenRequestData->password = 'password';

$accessTokenApi = new AccessTokenApi();
$accessTokenApi->httpClient = $httpClientFactory->create();

$response = $accessTokenApi->post($accessTokenRequestData);

echo $response->accessToken;
```

<small>**Note**: The code samples shown above are for illustrative
purposes only.</small>

## Frequently Asked Questions (FAQ)

<dl>
    <dt><strong>Problem</strong>: The following error message is
displayed while installing the library:
        <pre>wagento/gunbroker-connector requires php-http/client-implementation ^1.0 -> no matching package found</pre>
    </dt>
    <dd><strong>Solution</strong>: Install a
<a href="http://docs.php-http.org/en/latest/clients.html">HTTPlug adapter</a>
that is compatible with your HTTP client library (e.g. 
<code>php-http/guzzle7-adapter</code> for Guzzle 7).</dd>
</dl>

## Support

If you experience any issues or errors while using this library, please
open a ticket by sending an e-mail to support@wagento.com. Be sure to
include your PHP version, Composer version, a detailed description of
the problem including steps to reproduce it, and any other relevant
information. We do our best to respond to all legitimate inquires
within 48 business hours.

## License

The source code contained in this library is licensed under the
MIT License. A full copy of the license can be found in the included
[LICENSE](./LICENSE) file.

## History

A full history of the extension can be found in the
[CHANGELOG.md](./CHANGELOG.md) file.

## TODO

- Use the [Symfony Validation component](https://symfony.com/doc/current/validation.html)
to validate API handler and API object properties
- Improve test coverage and add missing tests