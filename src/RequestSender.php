<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi;

use Http\Client\Common\HttpMethodsClientInterface;
use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Exception\ConnectorException;
use Wagento\GunBrokerApi\Exception\InvalidRequestMethodException;

use function count;
use function is_subclass_of;
use function method_exists;
use function property_exists;
use function ucfirst;

final class RequestSender implements RequestSenderInterface
{
    private HttpMethodsClientInterface $httpClient;
    private ApiFactory $apiFactory;

    public function __construct(HttpMethodsClientInterface $httpClient, ApiFactory $apiFactory = null)
    {
        $this->httpClient = $httpClient;
        $this->apiFactory = $apiFactory ?? new ApiFactory();
    }

    /**
     * @param mixed[] $additionalParameters
     * @return DataTransferObject|bool
     * @throws ConnectorException
     */
    public function sendRequest(
        string $apiEndpoint,
        string $httpMethod,
        ?DataTransferObject $requestParameters = null,
        array $additionalParameters = []
    ) {
        $requestHandler = $this->apiFactory->create($apiEndpoint);
        $class = "\Wagento\GunBrokerApi\Api\\{$apiEndpoint}";

        if (!is_subclass_of($requestHandler, 'Wagento\GunBrokerApi\HttpMethod\\' . ucfirst($httpMethod), true)) {
            throw new InvalidRequestMethodException(
                "HTTP method \"{$httpMethod}\" is not valid for class \"{$class}\"."
            );
        }

        $requestHandler->httpClient = $this->httpClient;

        if (!method_exists($requestHandler, $httpMethod)) {
            throw new InvalidRequestMethodException("Request method \"{$httpMethod}\" is not valid for \"{$class}\".");
        }

        if (count($additionalParameters) > 0) {
            foreach ($additionalParameters as $property => $value) {
                if (!property_exists($requestHandler, $property)) {
                    continue;
                }

                $requestHandler->{$property} = $value;
            }
        }

        if ($requestParameters !== null) {
            $response = $requestHandler->$httpMethod($requestParameters);
        } else {
            $response = $requestHandler->$httpMethod();
        }

        return $response;
    }
}
