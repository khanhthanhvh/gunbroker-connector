<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi;

use Spatie\DataTransferObject\DataTransferObject;

use function count;

class SettablePropertiesDataTransferObject extends DataTransferObject
{
    /**
     * @param mixed[] $parameters
     */
    public function __construct(array $parameters = [])
    {
        if (count($parameters) === 0) {
            return;
        }

        parent::__construct($parameters);
    }
}
