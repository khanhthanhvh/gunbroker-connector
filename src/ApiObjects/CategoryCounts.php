<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * A Category Count contains ID, Name, and Count for a category for a given GunBroker.com search. For example
 * https://api.gunbroker.com/items?keywords=glock will return a collection of CategoryCount objects ordered by count
 * descending for all categories containing the keyword(s).
 *
 * https://api.gunbroker.com/User/HelpObjects/CategoryCounts
 */
final class CategoryCounts extends DataTransferObject
{
    /**
     * ID of the category.
     */
    public int $id;
    /**
     * Name of the category.
     */
    public string $name;
    /**
     * Count of matching items for the category.
     */
    public int $count;
}
