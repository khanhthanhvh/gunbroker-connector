<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/HelpObjects/CategoryPath
 */
final class CategoryPath extends DataTransferObject
{
    /**
     * ID of the category.
     */
    public int $id;
    /**
     * Name of the category.
     */
    public string $name;
    /**
     * The Category Path of the child category
     */
    public ?CategoryPath $child;
}
