<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/HelpObjects/UserInfo
 */
final class UserInfo extends DataTransferObject
{
    public int $userID;
    public string $userName;
    public string $email;
    public string $companyName;
    public string $firstName;
    public string $lastName;
    public string $address1;
    public string $address2;
    public string $city;
    public string $state;
    public string $postalCode;
    public string $countryCode;
    public string $phone;
    public string $alternatePhone;
    public string $faxPhone;
    public bool $isBusinessAddress;
    public ?bool $over21;
}
