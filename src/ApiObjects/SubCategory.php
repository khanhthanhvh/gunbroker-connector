<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Contains the CategoryId, CategoryName, and a boolean value indicating if the category has SubCategories.
 *
 * https://api.gunbroker.com/User/HelpObjects/SubCategoryInfo
 */
final class SubCategory extends DataTransferObject
{
    /**
     * ID of the category.
     */
    public int $categoryId;
    /**
     * Name of the category.
     */
    public string $categoryName;
    /**
     * Indicates if the category has sub categories.
     */
    public bool $hasSubCategories;
}
