<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Contains a summary of the item's characteristics.
 *
 * https://api.gunbroker.com/User/HelpObjects/ItemCharacteristic
 */
final class ItemCharacteristic extends DataTransferObject
{
    /**
     * The characteristic name (e.g. Manufacturer, Model, Gauge, Finish, Barrel Length, etc.).
     */
    public string $characteristicName;
    /**
     * The characteristic value(s) (e.g. Mossberg, Colt 1911, 9mm, Black Phosphate Anodized Finish, 16.3, etc.)
     *
     * @var string[]
     */
    public array $characteristicValue;
}
