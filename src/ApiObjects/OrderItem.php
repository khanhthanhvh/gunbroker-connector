<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/HelpObjects/OrderItem
 */
final class OrderItem extends DataTransferObject
{
    public ?string $gsin = null;
    public string $gtin;
    public bool $isFFLRequired;
    public bool $isFixedPrice;
    public string $itemCondition;
    public int $itemID;
    public float $itemPrice;
    public float $itemSubTotal;
    public int $quantity;
    public float $salesTax;
    public float $salesTaxRate;
    public string $serialNumber;
    public int $shippingProfileID;
    public string $sku;
    public string $thumbnail;
    public string $title;
    public string $upc;
    public float $weight;
    /**
     * @var array<int, string>
     */
    public array $weightUnit;
}
