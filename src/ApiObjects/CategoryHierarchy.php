<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Contains the GunBroker.com Category ID, Category Name, and an array of child categories that are also Category
 * Hierarchy objects.
 *
 * https://api.gunbroker.com/User/HelpObjects/CategoryHierarchy
 */
final class CategoryHierarchy extends DataTransferObject
{
    /**
     * ID of the category.
     */
    public int $id;
    /**
     * Name of the category.
     */
    public string $name;
    /**
     * List containing the direct children of a category.
     *
     * @var \Wagento\GunBrokerApi\ApiObjects\CategoryHierarchy[]
     */
    public array $children;
    /**
     * Indicates whether an FFL is required by default for a category.
     * (0 = FFL is not required, 1 = FFL is required, 2 = No default)
     */
    public int $defaultffl;
}
