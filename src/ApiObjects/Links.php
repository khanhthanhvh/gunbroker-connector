<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * A Link helps with discoverability and resource availability. A link may represent an available action or server
 * resources like files, images, etc. Most GunBroker.com REST API responses contain a list of links.
 *
 * https://api.gunbroker.com/User/HelpObjects/Links
 */
final class Links extends DataTransferObject
{
    /**
     * The the fully qualified URL for the linked resource.
     */
    public string $href;
    /**
     * The relation or purpose of the link relative to the current resource.
     */
    public string $rel;
    /**
     * The HTTP Verb associated with the linked resource.
     */
    public string $verb;
    /**
     * The Content-Type associated with the expected response for the linked resource (ex: image/jpg).
     */
    public ?string $contentType;
    /**
     * The suggested title for the link if displayed in a UI.
     */
    public string $title;
}
