<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Represents a Message Response object returned from the GunBroker.com API.
 *
 * https://api.gunbroker.com/User/HelpObjects/MessageResponse
 */
final class MessageResponse extends DataTransferObject
{
    /**
     * A message intended for the end user.
     */
    public string $userMessage;
    /**
     * A message intended for the developer consuming the API. This message may contain technical information to aid in
     * development.
     */
    public string $developerMessage;
    /**
     * A positive number representing the enumeration for the status code returned.
     */
    public int $gbStatusCode;
    /**
     * A list of resources related to the Message Response.
     *
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
