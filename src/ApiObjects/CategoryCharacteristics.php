<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Contains the information needed to display characteristics for a category on the GunBroker.com listing page.
 *
 * https://api.gunbroker.com/User/HelpObjects/CategoryCharacteristics
 */
final class CategoryCharacteristics extends DataTransferObject
{
    /**
     * The label to display on the page next to the characteristic list.
     */
    public string $label;
    /**
     * The internal name of the characteristic.
     */
    public string $name;
    /**
     * Whether or not the characteristic is required.
     */
    public bool $required;
    /**
     * The sort order to display the characteristic in.
     */
    public int $sortOrder;
    /**
     * A list of characteristic values to display, in alphabetical order.
     *
     * @var \Wagento\GunBrokerApi\ApiObjects\IdValuePair[]
     */
    public array $values;
}
