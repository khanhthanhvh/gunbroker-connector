<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Contains the information needed to accept input for an item's characteristics via the listing or edit process.
 *
 * https://api.gunbroker.com/User/HelpObjects/Characteristics
 */
final class Characteristics extends DataTransferObject
{
    /**
     * The internal name of the characteristic.
     */
    public string $name;
    /**
     * The value for the selected characteristic.
     */
    public string $selectedValue;
}
