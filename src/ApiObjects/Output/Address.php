<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/AddressBookGet
 */
final class Address extends DataTransferObject
{
    public string $address1;
    public string $address2;
    public int $addressID;
    public int $addressType;
    public string $city;
    public string $countryCode;
    public bool $isDefault;
    public string $postalCode;
    public string $state;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
