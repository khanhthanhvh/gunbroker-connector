<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * @see https://api.gunbroker.com/User/Help/ItemsSellingGet
 */
final class ItemSelling extends DataTransferObject
{
    public float $autoAcceptPrice;
    public float $autoRejectPrice;
    public int $bidCount;
    public float $buyNowPrice;
    public bool $canOffer;
    public int $counter;
    public float $currentBid;
    public string $endingDateTime;
    public string $endingDateTimeUTC;
    public ?string $expirationDate = null;
    public float $fixedPrice;
    public bool $hasCounter;
    public bool $hasReserve;
    public bool $hasReserveBeenMet;
    public int $highestBidderID;
    public string $highestBidderUserName;
    public int $itemID;
    public float $reservePrice;
    public int $sellerID;
    public string $serialNumber;
    public string $sku;
    public float $startingBid;
    public ?string $thumbnailURL = null;
    public string $title;
    public int $watchersCount;
    public int $quantity;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
