<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ShipmentsGet
 */
final class Shipment extends DataTransferObject
{
    public string $cancelDate;
    public int $carrier;
    public string $dateCreated;
    public float $height;
    public string $form1508Url;
    public string $labelUrl;
    public float $length;
    public float $rackRate;
    public float $shippingCostUser;
    public string $shipmentDescription;
    public string $shippingClassDisplayName;
    public string $shippingClassName;
    public int $shipmentID;
    public string $toName;
    public string $trackingNumber;
    public float $weight;
    public float $weightUnit;
    public float $width;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
