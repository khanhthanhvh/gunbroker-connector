<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/Items
 */
final class ItemSearch extends DataTransferObject
{
    public float $maxPrice;
    public float $minPrice;
    public int $countReturned;
    public int $count;
    public int $pageIndex;
    public int $pageSize;
    public int $gbStatusCode;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Output\ItemSearchResult[]
     */
    public array $results;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\CategoryCounts[]
     */
    public array $categoryCounts;
    /**
     * @var mixed[]
     */
    public array $characteristicBreakdown;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
