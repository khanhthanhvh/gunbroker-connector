<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/BillingInformationGet
 */
final class BillingInformation extends DataTransferObject
{
    public float $accountBalance;
    public float $amountDue;
    public string $lastPaymentAmount;
    public string $lastPaymentDate;
    public string $primaryPaymentMethod;
    public string $secondaryPaymentMethod;
}
