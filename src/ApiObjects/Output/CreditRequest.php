<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/CreditRequestsGet
 */
final class CreditRequest extends DataTransferObject
{
    public UserSummary $buyer;
    public string $creditDateTime;
    public int $creditRequestType;
    public string $endingDateTime;
    public int $itemID;
    public string $itemTitle;
    public int $objectID;
    public string $reminderDateTime;
    public string $requestDateTime;
    public UserSummary $seller;
    public string $startDateTime;
    public int $status;
}
