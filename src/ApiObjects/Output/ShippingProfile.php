<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ShippingProfileTypeGet
 */
final class ShippingProfile extends DataTransferObject
{
    public int $shippingProfileID;
    public int $profileType;
    public int $weightUnit;
}
