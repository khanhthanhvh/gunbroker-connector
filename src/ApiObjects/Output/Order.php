<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/UserSummary /Help/OrdersGet
 */
final class Order extends DataTransferObject
{
    public int $orderID;
    public string $billToName;
    public string $billToAddress1;
    public string $billToAddress2;
    public string $billToCity;
    public string $billToState;
    public string $billToPostalCode;
    public string $billToCountryCode;
    public string $billToEmail;
    public string $billToPhone;
    public UserSummary $buyer;
    public string $buyerConfirmationDate;
    public string $buyerReviewDate;
    public string $cancelDate;
    public string $couponCode;
    public float $couponValue;
    public float $lostCashDiscount;
    public float $lostCashDiscountPercent;
    public string $fflNumber;
    public string $fflReceivedDate;
    public bool $hasBuyerLeftFeedback;
    public bool $hasSellerLeftFeedback;
    public bool $isItemInsured;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\OrderItem[]
     */
    public array $items;
    public string $lastModifiedDate;
    public float $miscCharge;
    public string $miscDescription;
    /**
     * @var array<int, string>
     */
    public array $paymentMethods;
    public string $paymentReceivedDate;
    public string $orderDate;
    public float $orderTotal;
    public float $salesTaxHandling;
    public float $salesTaxInsurance;
    public float $salesTaxMisc;
    public float $salesTaxTotal;
    public UserSummary $seller;
    public string $sellerReviewCompleteDate;
    public string $sellerOrderID;
    /**
     * @var array<int, string>
     */
    public array $shipCarrier;
    /**
     * @var array<int, string>
     */
    public array $shipClass;
    public float $shipCost;
    public string $shipDate;
    public string $shipFromName;
    public string $shipFromAddress1;
    public string $shipFromAddress2;
    public string $shipFromCity;
    public string $shipFromState;
    public string $shipFromPostalCode;
    public string $shipFromCountryCode;
    public string $shipFromEmail;
    public string $shipFromPhone;
    public float $shipHandlingCost;
    public float $shipInsuranceCost;
    public string $shipToName;
    public string $shipToAddress1;
    public string $shipToAddress2;
    public string $shipToCity;
    public string $shipToState;
    public string $shipToPostalCode;
    public string $shipToCountryCode;
    public string $shipToEmail;
    public string $shipToPhone;
    public string $shipTrackingNumber;
    /**
     * @var array<int, string>
     */
    public array $status;
    /**
     * @var array<\Wagento\GunBrokerApi\ApiObjects\Links>
     */
    public array $links;
}
