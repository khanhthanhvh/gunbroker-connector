<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output\Feedback;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/FeedbackSummary
 */
final class Summary extends DataTransferObject
{
    public bool $isUserRegistered;
    public int $negativeFeedback30Days;
    public int $negativeFeedback60Days;
    public int $negativeFeedback90Days;
    public int $negativeFeedback120Days;
    public int $negativeFeedback180Days;
    public int $negativeFeedback365Days;
    public int $negativeFeedbackPoints;
    public int $neutralFeedback30Days;
    public int $neutralFeedback180Days;
    public int $neutralFeedback365Days;
    public int $neutralFeedbackPoints;
    public int $positiveFeedback30Days;
    public int $positiveFeedback180Days;
    public int $positiveFeedback365Days;
    public ?float $positiveFeedbackPercentage;
    public int $positiveFeedbackPoints;
    public int $totalFeedbackPoints;
    public UserSummary $user;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
