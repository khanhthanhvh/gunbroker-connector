<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/ItemsNotWon
 */
final class ItemNotWon extends DataTransferObject
{
    public int $bids;
    public float $buyNowPrice;
    public string $endingDate;
    public bool $hasColor;
    public bool $isTitleBoldface;
    public int $itemID;
    public float $myMaxBid;
    public float $salePrice;
    public UserSummary $seller;
    public string $subTitle;
    public string $title;
    public string $titleColor;
    public string $thumbnailURL;
}
