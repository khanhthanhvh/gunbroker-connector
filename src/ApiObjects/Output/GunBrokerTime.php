<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/GunBrokerTime
 */
final class GunBrokerTime extends DataTransferObject
{
    public string $gunBrokerTime;
    public string $gunBrokerVersion;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
