<?php
/**
 * https://api.gunbroker.com/User/Help/ShippingProfileSearch
 */

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ShippingProfileGet
 * https://api.gunbroker.com/User/Help/ShippingProfileSearch
 */
final class ShippingProfileSearchResult extends DataTransferObject
{
    public int $shippingProfileID;
    public string $name;
    public string $profileType;
    public float $handlingCost;
    public string $insuranceType;
    public float $insuranceCost;
    public float $insurancePercentage;
    public int $weightUnit;
    public bool $isActive;
}
