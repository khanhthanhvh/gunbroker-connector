<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/Items
 */
final class ItemSearchResult extends DataTransferObject
{
    public int $itemID;
    public float $bidPrice;
    public int $bids;
    public float $buyNowPrice;
    public float $buyPrice;
    public bool $canOffer;
    public float $creditCardUpChargePercent;
    public bool $eligibleForImmediateCheckout;
    public string $endingDate;
    public bool $freeShippingAvailable;
    public string $gtin;
    public bool $hasBuyNow;
    public bool $hasColor;
    public ?bool $hasFreedomCoin;
    public bool $hasPictures;
    public bool $hasReserve;
    public bool $hasReserveBeenMet;
    public int $highBidderID;
    public bool $isFeaturedItem;
    public bool $isFixedPrice;
    public bool $isHighlighted;
    public bool $isShowCaseItem;
    public bool $isTitleBoldface;
    public string $mfgPartNumber;
    public float $price;
    public int $quantity;
    public ?int $relistedAsItemID;
    public UserSummary $seller;
    public string $serialNumber;
    public string $sku;
    public int $sponsoredListing;
    public bool $sponsoredOnsiteListing;
    public string $subTitle;
    public string $thumbnailURL;
    public string $title;
    public string $titleColor;
    /*** Undocumented fields ***/
    public bool $hasQuickLook;
    public string $timeLeft;
}
