<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/ItemsWatchedGet
 */
final class ItemsWatched extends DataTransferObject
{
    public float $buyNowPrice;
    public bool $canOffer;
    public float $creditCardUpChargePercent;
    public int $quantity;
    public int $bids;
    public string $endingDate;
    public bool $hasImmediateCheckout;
    public bool $hasBuyNow;
    public bool $hasColor;
    public bool $hasPictures;
    public bool $hasReserve;
    public bool $hasReserveBeenMet;
    public bool $isTitleBoldface;
    public bool $isFeaturedItem;
    public bool $isFixedPrice;
    public bool $isFreeShipping;
    public int $itemID;
    public float $price;
    public UserSummary $seller;
    public string $subTitle;
    public string $thumbnailURL;
    public string $timeLeft;
    public string $title;
    public string $titleColor;
}
