<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\OrderItem;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/OrdersWon?
 */
final class OrderWon extends DataTransferObject
{
    public int $orderID;
    /**
     * @var int[]
     */
    public array $itemIDs;
    public UserSummary $buyer;
    public UserSummary $seller;
    public string $sellerName;
    public string $sellerAddress1;
    public string $sellerAddress2;
    public string $sellerCity;
    public string $sellerState;
    public string $sellerPostalCode;
    public string $sellerCountry;
    public string $sellerPhone;
    public string $sellerEmail;
    public string $shipToName;
    public string $shipToAddress1;
    public string $shipToAddress2;
    public string $shipToCity;
    public string $shipToState;
    public string $shipToPostalCode;
    public string $shipToCountryCode;
    public string $shipToPhone;
    public string $shipToEmail;
    public bool $buyerConfirmed;
    public bool $buyerReviewed;
    public string $buyerReviewCompleteDateUTC;
    public string $couponCode;
    public float $couponValue;
    public bool $fflReceived;
    public string $fflReceivedDateUTC;
    public bool $itemShipped;
    public string $shipDateUTC;
    public string $lastModifiedDate;
    public float $lostCashDiscount;
    public float $lostCashDiscountPercent;
    public bool $orderCancelled;
    public bool $orderComplete;
    public string $orderDateUTC;
    public bool $paymentReceived;
    public string $paymentRecievedDateUTC;
    public bool $sellerReviewed;
    public string $sellerReviewCompleteDateUTC;
    public string $sellerForcedCompleteDateUTC;
    public string $layawayAcceptanceDateUTC;
    public float $shipCost;
    public float $shipHandlingCost;
    public float $shipInsuranceCost;
    public float $totalPrice;
    public OrderItem $orderItemsCollection;
}
