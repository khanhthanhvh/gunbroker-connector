<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/PicturesGet
 */
final class Pictures extends DataTransferObject
{
    public int $pictureID;
    public int $displayOrder;
    public string $fileName;
    public int $itemID;
    public string $pictureURL;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
