<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/ItemsSold
 */
final class ItemSold extends DataTransferObject
{
    public float $buyNowPrice;
    public float $currentBid;
    public int $bidCount;
    public UserSummary $buyer;
    public bool $buyerPostedFeedback;
    public string $buyerReceivedItemDate;
    public string $buyerSentFFLDate;
    public string $buyerSentPaymentDate;
    public float $fixedPrice;
    public bool $hasColor;
    public bool $isTitleBoldface;
    public int $itemID;
    public int $quantity;
    public bool $relisted;
    public float $reservePrice;
    public string $saleDate;
    public float $salePrice;
    public UserSummary $seller;
    public bool $sellerPostedFeedback;
    public string $sellerReceivedPaymentDate;
    public string $sellerReceivedFFLDate;
    public string $sellerSentItemDate;
    public string $serialNumber;
    public string $sku;
    public string $subTitle;
    public string $title;
    public string $titleColor;
    public string $thumbnailURL;
    public float $totalPrice;
    public string $expirationDate;
}
