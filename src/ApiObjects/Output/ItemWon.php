<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/ItemsWon
 */
final class ItemWon extends DataTransferObject
{
    public bool $canOffer;
    public string $endingDate;
    public float $bidPrice;
    public bool $hasColor;
    public bool $isTitleBoldface;
    public int $itemID;
    public int $quantity;
    public UserSummary $seller;
    public string $subTitle;
    public string $thumbnailURL;
    public string $title;
    public string $titleColor;
    public float $totalPrice;
    public bool $didBuyerLeaveFeedback;
    public bool $didSellerLeaveFeedback;
    public bool $didBuyerReceiveItem;
    public bool $didSellerSendItem;
    public bool $didBuyerSendFFL;
    public bool $didSellerReceiveFFL;
    public bool $didBuyerSendPayment;
    public bool $didSellerReceivePayment;
}
