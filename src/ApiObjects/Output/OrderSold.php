<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/OrdersSold
 */
final class OrderSold extends DataTransferObject
{
    public UserSummary $buyer;
    public bool $buyerConfirmed;
    public bool $buyerReviewed;
    public bool $canMarkPaymentNotReceived;
    public ?string $couponCode = null;
    public float $couponValue;
    public ?string $eFFLUrl = null;
    public ?int $fflid = null;
    public bool $fflReceived;
    /**
     * @var int[]
     */
    public array $itemIDs;
    public bool $canRelist;
    public float $relistPrice;
    public bool $itemShipped;
    public string $lastModifiedDate;
    public float $lostCashDiscount;
    public float $lostCashDiscountPercent;
    public float $miscCharge;
    public bool $onLayaway;
    public bool $orderCancelled;
    public bool $orderComplete;
    public bool $orderReturned;
    public string $orderDate;
    public string $orderDateUTC;
    public bool $paymentInProcess;
    public string $sellerReviewCompleteDateUTC;
    public string $buyerConfirmationDateUTC;
    public ?string $paymentInProcessDateUTC = null;
    public string $paymentReceivedDateUTC;
    public ?string $fflReceivedDateUTC = null;
    public ?string $shipDateUTC = null;
    public ?string $sellerForcedCompleteDateUTC = null;
    public ?string $layawayAcceptanceDateUTC = null;
    public ?string $returnedDateUTC = null;
    public int $orderID;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\OrderItem[]
     */
    public array $orderItemsCollection;
    public ?float $paymentLogAmount = null;
    public ?bool $paymentLogCleared = null;
    public ?string $paymentLogDateTime = null;
    public ?string $paymentLogResponseCode = null;
    public ?string $paymentLogTransactionID = null;
    public bool $paymentReceived;
    public UserSummary $seller;
    public bool $sellerReviewed;
    public ?int $shipTerms = null;
    public string $shipToAddress1;
    public string $shipToAddress2;
    public string $shipToCity;
    public string $shipToCountryCode;
    public string $shipToName;
    public string $shipToPostalCode;
    public string $shipToState;
    public string $shipToPhone;
    public string $shipToEmail;
    public string $billToName;
    public string $billToAddress1;
    public string $billToAddress2;
    public string $billToCity;
    public string $billToState;
    public string $billToPostalCode;
    public string $billToCountryCode;
    public string $billToPhone;
    public string $billToEmail;
    public float $shipCost;
    public float $shipHandlingCost;
    public float $shipInsuranceCost;
    public bool $taxExempt;
    public float $totalPrice;
    public float $complianceFee;
    public float $salesTaxTotal;
    public ?string $taxCloudCartID = null;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
