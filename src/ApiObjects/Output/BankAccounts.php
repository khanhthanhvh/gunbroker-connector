<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/BankAccountsGET
 */
final class BankAccounts extends DataTransferObject
{
    public string $accountNumber;
    public int $accountType;
    public string $bankName;
    public string $nickname;
    public int $objectID;
    public string $routingNumber;
    public int $status;
    public int $userId;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
