<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/FFLsID
 * https://api.gunbroker.com/User/Help/FFLs
 * https://api.gunbroker.com/User/Help/FFLsZip
 */
final class FFL extends DataTransferObject
{
    public int $fflID;
    public string $address1;
    public string $address2;
    public ?string $cellPhone;
    public string $city;
    public string $company;
    public ?string $fax;
    public ?string $handGunDescription;
    public ?float $handGunFee;
    public ?string $hours;
    public float $latitude;
    public float $longitude;
    public ?string $longGunDescription;
    public float $longGunFee;
    public ?string $licenseNumber;
    public string $name;
    public ?string $nicsDescription;
    public ?float $nicsFee;
    public ?string $otherDescription;
    public ?float $otherFee;
    public ?string $otherPhone;
    public ?string $phone;
    public ?string $promotionalText;
    public string $state;
    public ?string $website;
    public string $zip;
    /** @var \Wagento\GunBrokerApi\ApiObjects\Links[] $links */
    public array $links;
    /*** Undocumented ***/
    public float $distance;
    public bool $licenseOnFile;
}
