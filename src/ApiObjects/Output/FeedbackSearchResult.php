<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/Feedback
 */
final class FeedbackSearchResult extends DataTransferObject
{
    public int $feedbackID;
    public int $itemID;
    public string $buyerSellerStatus;
    public bool $canDeleteFeedback;
    public bool $canDeleteReplyFeedback;
    public bool $canDeleteFollowUpFeedback;
    public bool $canEditFeedback;
    public bool $canEditReplyFeedback;
    public bool $canEditFollowUpFeedback;
    public bool $canFollowUpFeedback;
    public bool $canReplyFeedback;
    public string $comment;
    public string $date;
    public ?string $followUp = null;
    public ?string $followUpDate = null;
    public bool $isRegistered;
    public UserSummary $fromUser;
    public UserSummary $toUser;
    public string $ratingLetter;
    public float $ratingScore;
    public ?string $reply = null;
    public ?string $replyDate = null;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
