<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output\Users;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\FFLSummary;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/UsersAccountInfoGet
 */
final class AccountInfo extends DataTransferObject
{
    public string $firstName;
    public string $lastName;
    public string $address1;
    public ?string $address2 = null;
    public string $city;
    public string $state;
    public string $postalCode;
    public string $countryCode;
    public string $primaryPhone;
    public string $secondaryPhone;
    public string $fax;
    public bool $useBusiness;
    public ?string $businessCompany = null;
    public ?string $businessFirstName = null;
    public ?string $businessLastName = null;
    public ?string $businessAddress1 = null;
    public ?string $businessAddress2 = null;
    public ?string $businessCity = null;
    public ?string $businessState = null;
    public ?string $businessPostalCode = null;
    public ?string $businessCountryCode = null;
    public ?string $businessPrimaryPhone = null;
    public ?string $businessSecondaryPhone = null;
    public ?string $businessFax = null;
    public string $publicEmailAddress;
    public string $registrationEmailAddress;
    public UserSummary $userSummary;
    public FFLSummary $fflSummary;
}
