<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output\Users;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/AccessTokenPost
 */
final class AccessToken extends DataTransferObject
{
    public string $accessToken;
}
