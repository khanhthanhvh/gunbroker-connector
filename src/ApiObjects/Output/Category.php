<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\CategoryPath;

/**
 * https://api.gunbroker.com/User/Help/CategoriesID
 */
final class Category extends DataTransferObject
{
    public int $categoryID;
    public string $categoryName;
    public bool $canContainItems;
    public string $description;
    public int $count;
    public CategoryPath $categoryPath;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\SubCategory[]
     */
    public array $subCategories;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
