<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObjectCollection;

/**
 * @method Item current()
 * @method void offsetSet(int $offset, Item $value)
 * @method Item offsetGet(int $offset)
 * @method Item[] toArray()
 * @method Item[] items()
 *
 * https://api.gunbroker.com/User/Help/ItemsItemIDs
 */
final class Items extends DataTransferObjectCollection
{
    /**
     * @param mixed[] $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct(Item::arrayOf($data));
    }
}
