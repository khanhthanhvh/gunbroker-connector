<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/SavedSearchesGet
 */
final class SavedSearch extends DataTransferObject
{
    public int $id;
    public string $name;
    public string $description;
    public string $emailSettings;
    public bool $willBeSentDailyEmail;
    public bool $includeRelistsInEmail;
    public int $sortByValue;
}
