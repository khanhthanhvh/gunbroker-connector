<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsUnsold
 */
final class ItemUnsold extends DataTransferObject
{
    public int $bidCount;
    public float $buyNowPrice;
    public int $categoryID;
    public string $endDate;
    public float $fixedPrice;
    public bool $hasColor;
    public float $highBid;
    public bool $isTitleBoldface;
    public int $itemID;
    public int $quantity;
    public bool $relisted;
    public float $reservePrice;
    public string $serialNumber;
    public string $sku;
    public string $startDate;
    public float $startingBid;
    public float $startingPrice;
    public string $subTitle;
    public ?string $thumbnailURL;
    public string $title;
    public string $titleColor;
    public string $upc;
    public int $watchersCount;
    public ?string $expirationDate;
    public bool $hasBuyNow;
    public string $itemTitle;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
