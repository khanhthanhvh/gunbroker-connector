<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/SavedSearchesGet
 */
final class SavedSearches extends DataTransferObject
{
    public int $count;
    public int $pageIndex;
    public int $pageSize;
    public int $gbStatusCode;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Output\SavedSearch[]
     */
    public array $results;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
