<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsScheduled
 */
final class ItemScheduled extends DataTransferObject
{
    public float $buyNowPrice;
    public bool $canOffer;
    public string $endingDate;
    public float $fixedPrice;
    public bool $hasBuyNow;
    public bool $hasColor;
    public bool $hasReserve;
    public bool $isFixedPrice;
    public bool $isTitleBoldface;
    public int $itemID;
    public int $quantity;
    public float $reservePrice;
    public string $serialNumber;
    public string $sku;
    public float $startingBid;
    public string $startingDate;
    public string $subTitle;
    public string $title;
    public string $titleColor;
    public string $expirationDate;
}
