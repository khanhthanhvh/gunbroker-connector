<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output\FraudClaims;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/FraudClaimsSellerGet
 */
final class Seller extends DataTransferObject
{
    public int $objectID;
    public int $itemID;
    public string $itemTitle;
    public UserSummary $buyer;
    public UserSummary $seller;
    public int $status;
    public string $dateTime;
    public string $lastModifiedDate;
}
