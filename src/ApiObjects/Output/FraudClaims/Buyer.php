<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output\FraudClaims;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/FraudClaimsBuyerGet
 */
final class Buyer extends DataTransferObject
{
    public int $objectID;
    public int $itemID;
    public string $itemTitle;
    public UserSummary $buyer;
    public UserSummary $seller;
    public int $status;
    public string $dateTime;
    public string $lastModifiedDate;
    public string $proofOfPaymentDate;
    public string $claimFormMailed;
    public int $proofOfPaymentOptions;
    public int $ic3Options;
    public int $ccDenialOptions;
    public int $uspsOptions;
    public int $copyOfAuctionOptions;
    public int $policeOptions;
    public int $gunsmithOptions;
}
