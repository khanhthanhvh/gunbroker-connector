<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsBidOn
 */
final class ItemsBidOn extends DataTransferObject
{
    public int $count;
    public int $pageIndex;
    public int $pageSize;
    public int $gbStatusCode;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Output\ItemBidOn[]
     */
    public array $results;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
