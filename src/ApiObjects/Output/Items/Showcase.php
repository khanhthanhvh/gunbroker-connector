<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output\Items;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/ItemsShowcase
 */
final class Showcase extends DataTransferObject
{
    public int $bids;
    public float $buyNowPrice;
    public bool $canOffer;
    public bool $eligibleForImmediateCheckout;
    public string $endingDate;
    public string $gtin;
    public bool $hasBuyNow;
    public bool $hasColor;
    public ?bool $hasFreedomCoin;
    public bool $hasPictures;
    public bool $hasReserve;
    public bool $hasReserveBeenMet;
    public bool $isFeaturedItem;
    public bool $isFixedPrice;
    public bool $isHighlighted;
    public bool $isShowCaseItem;
    public bool $isTitleBoldface;
    public int $itemID;
    public string $mfgPartNumber;
    public float $price;
    public int $quantity;
    public UserSummary $seller;
    public string $sku;
    public int $sponsoredListing;
    public bool $sponsoredOnsiteListing;
    public string $subTitle;
    public string $thumbnailURL;
    public string $title;
    public string $titleColor;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[]
     */
    public array $links;
}
