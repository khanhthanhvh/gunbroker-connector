<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/ItemsBidOn
 */
final class ItemBidOn extends DataTransferObject
{
    public float $currentBid;
    public bool $canOffer;
    public bool $hasBuyNow;
    public float $buyNowPrice;
    public string $endingDateTime;
    public bool $hasColor;
    public bool $isTitleBoldface;
    public int $itemID;
    public bool $isWinningBidder;
    public float $myMaxBid;
    public UserSummary $seller;
    public string $subTitle;
    public string $thumbnailURL;
    public string $title;
    public string $titleColor;
}
