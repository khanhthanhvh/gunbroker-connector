<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\UserSummary;

/**
 * https://api.gunbroker.com/User/Help/ItemsID
 * https://api.gunbroker.com/User/Help/ItemsItemIDs
 */
final class Item extends DataTransferObject
{
    public int $itemID;
    public float $bidIncrement;
    public int $bids;
    public float $buyNowPrice;
    public ?UserSummary $buyer;
    public float $buyPrice;
    public float $cashDiscount;
    public int $categoryID;
    public string $categoryName;
    public bool $collectsSalesTax;
    /**
     * @var array<int, string>
     */
    public array $condition;
    public float $currentBid;
    public bool $canOffer;
    public string $description;
    public string $descriptionOnly;
    public bool $eligibleForImmediateCheckout;
    public string $endingDate;
    public float $fixedPrice;
    public string $gtin;
    public bool $hasBuyNow;
    public bool $hasColor;
    public bool $hasPictures;
    public bool $hasReserve;
    public bool $hasReserveBeenMet;
    public bool $hasStarted;
    public bool $hasWinningBid;
    public bool $isHighlighted;
    public bool $isShowCaseItem;
    public bool $isTitleBoldface;
    public bool $isWatching;
    /**
     * @var array<int, string>
     */
    public array $inspectionPeriod;
    public bool $isActive;
    public bool $isCurrentUserHighBidder;
    public bool $isFFLRequired;
    public bool $isFeaturedItem;
    public bool $isFixedPrice;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\ItemCharacteristic[]
     */
    public array $itemCharacteristics;
    public string $location;
    public string $mfgPartNumber;
    public float $minimumBid;
    /**
     * @var array<int, string>
     */
    public array $paymentMethods;
    /**
     * @var array<string>
     */
    public array $pictures;
    public string $prop65Warning;
    public int $quantity;
    public int $relistedAsItemID;
    /**
     * @var array<string, float>
     */
    public array $salesTaxes;
    public UserSummary $seller;
    public string $serialNumber;
    /**
     * @var array|array<int, string>
     */
    public array $shippingClassCosts;
    /**
     * @var array<int, string>
     */
    public array $shippingClassesSupported;
    public string $sku;
    public string $standardText;
    public int $standardTextVersionID;
    public float $startingBid;
    public string $startingDate;
    public string $subTitle;
    public string $thumbnailURL;
    public string $title;
    public string $titleColor;
    public string $upc;
    public int $viewCounter;
    public ?float $weight = null;
    /**
     * @var array<int, string>
     */
    public array $weightUnit;
    /**
     * @var array<int, string>
     */
    public array $whoPaysForShipping;
    public bool $willShipInternational;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Links[] $links
     */
    public array $links;
    /*** Undocumented fields ***/
    public bool $freeShippingAvailable;
    public float $bidPrice;
    public bool $hasQuickLook;
    public string $timeLeft;
}
