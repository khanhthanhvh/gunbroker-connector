<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Output;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/SEOData
 */
final class SEOData extends DataTransferObject
{
    public string $bodyCopy;
    public string $metaCopy;
    public string $h1Copy;
    public string $titleCopy;
}
