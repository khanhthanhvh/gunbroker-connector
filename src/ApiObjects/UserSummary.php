<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * The User Summary for buyers and sellers.
 *
 * https://api.gunbroker.com/User/HelpObjects/UserSummary
 */
final class UserSummary extends DataTransferObject
{
    /**
     * The User ID.
     */
    public int $userID;
    /**
     * The account status for the user.
     */
    public int $accountStatus;
    /**
     * The User's feedback rating and feedback count (e,g, A+(300)).
     */
    public string $feedbackRating;
    /**
     * Whether or not the User is an FFL Dealer.
     */
    public bool $isFFL;
    /**
     * Whether or not the User is a Charter Gold Member.
     */
    public bool $isCharterGoldMember;
    /**
     * Whether or not the User is a Gold Member.
     */
    public bool $isGoldMember;
    /**
     * Whether or not the User is a valid registered user.
     */
    public bool $isRegisteredUser;
    /**
     * Whether or not the User is a Top 10 seller.
     */
    public bool $isTop10Seller;
    /**
     * Whether or not the User is a Top 100 seller.
     */
    public bool $isTop100Seller;
    /**
     * Whether or not the User is a Top 25 seller.
     */
    public bool $isTop25Seller;
    /**
     * Whether or not the User is a GunBroker.com Verified User.
     */
    public bool $isVerified;
    /**
     * The User's Username.
     */
    public string $username;
    /**
     * (Optional) The User's Options flags.
     */
    public ?int $userOptions;
    /**
     * Date user joined site as a UTC ISO-8601 timestamp.
     */
    public string $memberSince;
    /**
     * Whether or not the user is 21 years old.
     */
    public bool $over21;
}
