<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Represents a generic ID and value.
 *
 * https://api.gunbroker.com/User/HelpObjects/IDValuePair
 */
final class IdValuePair extends DataTransferObject
{
    /**
     * A positive number if used, otherwise -1.
     */
    public int $id;
    /**
     * A string value.
     */
    public string $value;
}
