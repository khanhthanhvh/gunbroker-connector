<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/OrdersSold
 */
final class OrdersSold extends SettablePropertiesDataTransferObject
{
    public ?int $buyerUserID = null;
    public ?string $buyerUserName = null;
    public ?int $fflStatus = null;
    public ?int $itemID = null;
    public ?string $keywords = null;
    public ?string $ordersModifiedSinceDate = null;
    public ?int $orderStatus = null;
    public ?int $sort = null;
    public ?int $sortOrder = null;
    public ?int $timeFrame = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
