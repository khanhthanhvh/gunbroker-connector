<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input\Users;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/UsersContactInfoGet
 */
final class ContactInfo extends SettablePropertiesDataTransferObject
{
    public ?int $userID = null;
    public ?string $userName = null;
}
