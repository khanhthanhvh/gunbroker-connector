<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input\Users;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/AccessTokenPost
 */
final class AccessToken extends SettablePropertiesDataTransferObject
{
    public string $username;
    public string $password;
}
