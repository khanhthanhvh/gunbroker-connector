<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input\Items;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsShowcase
 */
final class Showcase extends SettablePropertiesDataTransferObject
{
    public int $count;
    public string $keywords;
}
