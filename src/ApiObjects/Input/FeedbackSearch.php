<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/Feedback
 */
final class FeedbackSearch extends SettablePropertiesDataTransferObject
{
    public ?int $itemID = null;
    public ?int $feedbackSearchBy = null;
    public ?int $feedbackSearchType = null;
    public ?int $fromUserID = null;
    public ?int $sort = null;
    public ?int $timeframe = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
