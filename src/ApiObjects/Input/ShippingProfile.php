<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ShippingProfileTypeGet
 */
final class ShippingProfile extends SettablePropertiesDataTransferObject
{
    public int $shippingProfileID;
}
