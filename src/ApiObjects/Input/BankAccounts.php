<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/BankAccountsGET
 */
final class BankAccounts extends SettablePropertiesDataTransferObject
{
    public ?string $bankName = null;
    public ?string $nickname = null;
    public ?int $sort = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
