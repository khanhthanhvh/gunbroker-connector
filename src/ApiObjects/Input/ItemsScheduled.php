<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsScheduled
 */
final class ItemsScheduled extends SettablePropertiesDataTransferObject
{
    public ?bool $canOffer = null;
    public ?int $itemID = null;
    public ?string $keywords = null;
    public ?string $serialNumber = null;
    public ?string $sku = null;
    public ?int $sort = null;
    public ?int $sortOrder = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
