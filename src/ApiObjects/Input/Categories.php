<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/Categories
 */
final class Categories extends SettablePropertiesDataTransferObject
{
    public ?string $searchTerm = null;
    public ?int $sortType = null;
    public ?int $parentCategoryID = null;
    public ?bool $showOnlyFirstLevelSubCategories = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
