<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsPut
 */
final class UpdatedItem extends SettablePropertiesDataTransferObject
{
    public ?float $autoAcceptPrice = null;
    public ?float $autoRejectPrice = null;
    public ?float $buyNowPrice = null;
    public ?bool $canOffer = null;
    public ?bool $cancelOpenOffers = null;
    public ?string $description = null;
    public ?float $fixedPrice = null;
    public ?string $gtin = null;
    public ?string $mfgPartNumber = null;
    public ?string $prop65Warning = null;
    public ?int $quantity = null;
    public ?float $reservePrice = null;
    public ?string $sku = null;
    public ?int $standardTextID = null;
    public ?float $startingBid = null;
    public ?string $subTitle = null;
    public ?string $title = null;
    public ?string $upc = null;
}
