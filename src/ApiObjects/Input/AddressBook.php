<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/AddressBookGet
 */
final class AddressBook extends SettablePropertiesDataTransferObject
{
    public ?string $address1 = null;
    public ?string $address2 = null;
    public ?string $city = null;
    public ?string $state = null;
    public ?string $postalCode = null;
    public ?int $sort = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
