<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/PicturesGet
 */
final class Pictures extends SettablePropertiesDataTransferObject
{
    public int $itemID;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
