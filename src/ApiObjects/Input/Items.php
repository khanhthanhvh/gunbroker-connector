<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsItemIDs
 */
final class Items extends SettablePropertiesDataTransferObject
{
    public string $itemIds;
}
