<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/SavedSearchesPost
 */
final class SaveSearch extends SettablePropertiesDataTransferObject
{
    public string $name;
    public ?bool $includeRelistsInEmail = null;
    public ?bool $saveSearchDailyEmailNotification = null;
    public ?bool $buyNowOnly = null;
    public ?bool $canAcceptFreedomCoin = null;
    public ?bool $canOffer = null;
    public ?string $categories = null;
    public ?int $condition = null;
    public ?string $country = null;
    public ?bool $eligibleForImmediateCheckout = null;
    public ?string $exclude = null;
    public ?bool $featuredListing = null;
    public ?bool $hasPictures = null;
    public ?string $includeSellers = null;
    public ?string $keywords = null;
    public ?int $keywordOperator = null;
    public ?int $maxBidCount = null;
    public ?int $minBidCount = null;
    public ?float $maxPrice = null;
    public ?float $minPrice = null;
    public ?float $maxStartingBid = null;
    public ?float $minStartingBid = null;
    public ?bool $noCCFees = null;
    public ?bool $noReserveItems = null;
    public ?int $pageSize = null;
    public ?string $sellerName = null;
    public ?string $state = null;
    public ?int $sort = null;
    public ?int $timeframe = null;
    public ?string $upc = null;
    public ?bool $willShipInternational = null;
}
