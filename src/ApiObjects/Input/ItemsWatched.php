<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsWatchedGet
 */
final class ItemsWatched extends SettablePropertiesDataTransferObject
{
    public ?bool $canOffer = null;
    public ?int $itemStatus = null;
    public ?string $keywords = null;
    public ?string $sellerName = null;
    public ?int $sort = null;
    public ?int $sortOrder = null;
    public ?int $timeFrame = null;
}
