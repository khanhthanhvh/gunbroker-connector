<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsSellingGet
 */
final class ItemsSelling extends SettablePropertiesDataTransferObject
{
    public ?float $autoAcceptPrice = null;
    public ?float $autoRejectPrice = null;
    public ?bool $buyNowOnly = null;
    public ?bool $canOffer = null;
    public ?bool $fixedPriceOnly = null;
    public ?bool $hasBids = null;
    public ?bool $hasNoWinningBid = null;
    public ?int $highestBidderUserID = null;
    public ?string $highestBidderUserName = null;
    public ?int $itemID = null;
    public ?string $keywords = null;
    public ?int $sort = null;
    public ?int $timeFrame = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
