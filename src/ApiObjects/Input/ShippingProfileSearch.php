<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ShippingProfileSearch
 */
final class ShippingProfileSearch extends SettablePropertiesDataTransferObject
{
    public ?string $name = null;
    public ?bool $active = null;
    public ?int $sort = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
