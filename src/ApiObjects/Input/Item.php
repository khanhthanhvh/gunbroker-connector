<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\ApiObjects\Input\Item\PaymentMethods;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\PremiumFeatures;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\ShippingClassCosts;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\ShippingClassesSupported;
use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsPost
 */
final class Item extends SettablePropertiesDataTransferObject
{
    public float $autoAcceptPrice;
    public float $autoRejectPrice;
    public int $autoRelist;
    public ?int $autoRelistFixedCount = null;
    public ?float $buyNowPrice = null;
    public bool $canOffer;
    public int $categoryID;
    public ?bool $collectTaxes = null;
    public int $condition;
    public string $countryCode;
    public string $description;
    public ?float $fixedPrice = null;
    public ?string $gtin = null;
    public ?int $inspectionPeriod = null;
    public bool $isFFLRequired;
    public int $listingDuration;
    public string $mfgPartNumber;
    public PaymentMethods $paymentMethods;
    /**
     * @var string[]|null
     */
    public ?array $pictureURLs = null;
    public ?PremiumFeatures $premiumFeatures = null;
    public string $postalCode;
    public ?string $prop65Warning = null;
    public int $quantity;
    public ?float $reservePrice = null;
    /**
     * @var \Wagento\GunBrokerApi\ApiObjects\Input\Item\SalesTaxes[]|null
     */
    public ?array $salesTaxes = null;
    public ?bool $useDefaultSalesTax = null;
    public ?string $serialNumber = null;
    public ?ShippingClassCosts $shippingClassCosts = null;
    public ?ShippingClassesSupported $shippingClassesSupported = null;
    public ?int $shippingProfileID = null;
    public ?string $sku = null;
    public int $standardTextID;
    public ?float $startingBid = null;
    public string $title;
    public ?string $upc = null;
    public ?float $weight = null;
    public ?int $weightUnit = null;
    public int $whoPaysForShipping;
    public bool $willShipInternational;
}
