<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/SEOData
 */
final class SEOData extends SettablePropertiesDataTransferObject
{
    public string $urlName;
    public string $keywords;
}
