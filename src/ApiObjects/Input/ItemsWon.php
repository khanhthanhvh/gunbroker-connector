<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsWon
 */
final class ItemsWon extends SettablePropertiesDataTransferObject
{
    public ?bool $canOffer = null;
    public ?string $sellerName = null;
    public ?string $keywords = null;
    public ?int $itemStatus = null;
    public ?int $sort = null;
    public ?int $sortOrder = null;
    public ?int $timeFrame = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
