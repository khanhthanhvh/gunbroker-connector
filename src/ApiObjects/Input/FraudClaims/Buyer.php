<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input\FraudClaims;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/FraudClaimsBuyerGet
 */
final class Buyer extends SettablePropertiesDataTransferObject
{
    public ?int $itemID = null;
    public ?string $itemTitle = null;
    public ?string $sellerName = null;
    public ?int $sort = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
