<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/Items
 */
final class ItemSearch extends SettablePropertiesDataTransferObject
{
    public ?bool $buyNowOnly = null;
    public ?bool $canAcceptFreedomCoin = null;
    public ?bool $canOffer = null;
    /**
     * @var array<int>
     */
    public ?array $categories = null;
    public ?int $condition = null;
    public ?string $country = null;
    public ?bool $eligibleForImmediateCheckout = null;
    public ?string $exclude = null;
    public ?bool $featuredListing = null;
    public ?bool $fixedPriceOnly = null;
    public ?bool $hasPictures = null;
    public ?string $includeSellers = null;
    public ?string $itemIDList = null;
    public ?string $keywords = null;
    public ?int $keywordOperator = null;
    public ?int $maxBidCount = null;
    public ?int $minBidCount = null;
    public ?float $maxStartingBid = null;
    public ?float $minStartingBid = null;
    public ?bool $noCCFees = null;
    public ?bool $noReserveItems = null;
    public ?int $pageOrder = null;
    public ?string $sellerName = null;
    public ?string $sku = null;
    public ?int $sort = null;
    public ?int $sponsoredListing = null;
    public ?bool $sponsoredOnsiteListing = null;
    public ?string $state = null;
    public ?int $timeframe = null;
    public ?string $upc = null;
    public ?bool $willShipInternational = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
