<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/FFLs
 */
final class FFLs extends SettablePropertiesDataTransferObject
{
    public ?float $latitude = null;
    public ?float $longitude = null;
    public ?float $radius = null;
    public ?string $state = null;
    public ?bool $detailed = null;
    public ?int $sort = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
