<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input\Orders;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/OrdersShippingPut
 */
final class Shipping extends SettablePropertiesDataTransferObject
{
    public string $trackingNumber;
    public int $carrier;
}
