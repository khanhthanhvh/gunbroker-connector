<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input\Orders;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/OrdersFlagsPut
 */
final class Flags extends SettablePropertiesDataTransferObject
{
    public ?bool $cancelOrder = null;
    public ?bool $fflReceived = null;
    public ?bool $forceComplete = null;
    public ?bool $orderShipped = null;
    public ?bool $paymentReceived = null;
}
