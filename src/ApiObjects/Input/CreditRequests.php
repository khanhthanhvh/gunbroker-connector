<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/CreditRequestsGet
 */
final class CreditRequests extends SettablePropertiesDataTransferObject
{
    public ?string $buyerUserName = null;
    public ?string $sellerUserName = null;
    public ?int $itemID = null;
    public ?int $sort = null;
    public ?int $status = null;
    public ?int $timeFrame = null;
    public ?int $type = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
