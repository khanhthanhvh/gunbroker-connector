<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input\Item;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsPost
 */
final class ShippingClassCosts extends SettablePropertiesDataTransferObject
{
    public ?float $overnight = null;
    public ?float $twoDay = null;
    public ?float $threeDay = null;
    public ?float $ground = null;
    public ?float $firstClass = null;
    public ?float $priority = null;
    public ?float $other = null;
}
