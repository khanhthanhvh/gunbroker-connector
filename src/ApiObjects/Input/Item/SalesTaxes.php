<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input\Item;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsPost
 */
final class SalesTaxes extends SettablePropertiesDataTransferObject
{
    public string $state;
    public string $taxRate;
}
