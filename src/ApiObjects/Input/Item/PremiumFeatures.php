<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input\Item;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsPost
 */
final class PremiumFeatures extends SettablePropertiesDataTransferObject
{
    public bool $hasViewCounter = false;
    public bool $isFeaturedItem = false;
    public bool $isHighlighted = false;
    public bool $isShowCaseItem = false;
    public bool $isTitleBoldface = false;
    public bool $isSponsoredOnsite = false;
    public ?string $scheduledStartingDate = null;
    public ?string $subTitle = null;
    public ?string $thumbnailURL = null;
    public ?string $titleColor = null;
}
