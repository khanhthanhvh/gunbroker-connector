<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input\Item;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsPost
 */
final class ShippingClassesSupported extends SettablePropertiesDataTransferObject
{
    public bool $overnight = false;
    public bool $twoDay = false;
    public bool $threeDay = false;
    public bool $ground = false;
    public bool $firstClass = false;
    public bool $priority = false;
    public bool $other = false;
}
