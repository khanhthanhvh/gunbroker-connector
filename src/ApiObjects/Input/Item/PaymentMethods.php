<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input\Item;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsPost
 */
final class PaymentMethods extends SettablePropertiesDataTransferObject
{
    public bool $check = false;
    public bool $visaMastercard = false;
    public bool $cod = false;
    public bool $escrow = false;
    public bool $amex = false;
    public bool $payPal = false;
    public bool $discover = false;
    public bool $seeItemDesc = false;
    public bool $certifiedCheck = false;
    public bool $uspsMoneyOrder = false;
    public bool $moneyOrder = false;
    public bool $freedomCoin = false;
}
