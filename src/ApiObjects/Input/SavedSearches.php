<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/SavedSearchesGet
 */
final class SavedSearches extends SettablePropertiesDataTransferObject
{
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
