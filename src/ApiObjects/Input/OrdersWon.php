<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/OrdersWon
 */
final class OrdersWon extends SettablePropertiesDataTransferObject
{
    public ?int $sellerUserID = null;
    public ?string $sellerUserName = null;
    public ?int $itemID = null;
    public ?string $ordersModifiedSinceDate = null;
    public ?int $orderStatus = null;
    public ?int $sort = null;
    public ?int $sortOrder = null;
    public ?int $timeFrame = null;
    public ?string $keywords = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
