<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsUnsold
 */
final class ItemsUnsold extends SettablePropertiesDataTransferObject
{
    public ?int $itemStatus = null;
    public ?int $sort = null;
    public ?int $sortOrder = null;
    public ?int $timeFrame = null;
    public ?string $serialNumber = null;
    public ?string $sku = null;
    public ?int $itemID = null;
    public ?int $categoryID = null;
    public ?string $keywords = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
