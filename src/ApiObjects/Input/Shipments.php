<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ShipmentsGet
 */
final class Shipments extends SettablePropertiesDataTransferObject
{
    public ?string $shipmentDescription = null;
    public ?int $shipmentID = null;
    public ?string $toName = null;
    public ?string $trackingNumber = null;
    public ?int $sort = null;
    public ?int $timeFrame = null;
}
