<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/ItemsWatchedPost
 */
final class WatchItem extends SettablePropertiesDataTransferObject
{
    public int $itemID;
}
