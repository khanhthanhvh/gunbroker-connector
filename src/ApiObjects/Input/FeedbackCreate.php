<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/FeedbackPost
 */
final class FeedbackCreate extends SettablePropertiesDataTransferObject
{
    public ?int $itemID = null;
    public ?string $comment = null;
    public ?int $rating = null;
}
