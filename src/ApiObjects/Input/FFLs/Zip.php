<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects\Input\FFLs;

use Wagento\GunBrokerApi\SettablePropertiesDataTransferObject;

/**
 * https://api.gunbroker.com/User/Help/FFLsZip
 */
final class Zip extends SettablePropertiesDataTransferObject
{
    public ?string $state = null;
    public ?float $radius = null;
    public ?int $partnerID = null;
    public ?bool $detailed = null;
    public ?int $sort = null;
    public ?int $pageIndex = null;
    public ?int $pageSize = null;
}
