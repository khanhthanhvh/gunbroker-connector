<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\ApiObjects;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * https://api.gunbroker.com/User/HelpObjects/FFLSummary
 */
final class FFLSummary extends DataTransferObject
{
    public int $objectID;
    public ?string $licenseNumber = null;
    public ?string $eFFLUrl = null;
    public ?string $name = null;
    public ?string $company = null;
    public ?string $address1 = null;
    public ?string $address2 = null;
    public ?string $city = null;
    public ?string $state = null;
    public ?string $zip = null;
    public ?string $phone = null;
    public ?string $fax = null;
    public ?string $cellPhone = null;
    public ?string $hours = null;
    public ?string $promo = null;
    public int $statusFlag;
    public ?bool $handGunFee = null;
    public ?string $handGunDesc = null;
    public ?bool $longGunFee = null;
    public ?string $longGunDesc = null;
    public ?bool $nicFee = null;
    public ?string $nicDesc = null;
    public ?bool $otherFee = null;
    public ?string $otherDesc = null;
    public ?bool $isActive;
}
