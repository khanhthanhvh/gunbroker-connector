<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Utils;

use Spatie\DataTransferObject\DataTransferObject;

use function is_array;

final class RecursiveNullFilter
{
    /**
     * @param mixed[] $data
     * @return mixed[]
     */
    public function __invoke(array $data): array
    {
        return self::filter($data);
    }

    /**
     * @param mixed[] $data
     * @return mixed[]
     */
    public static function filter(array $data): array
    {
        foreach ($data as $key => &$value) {
            if ($value instanceof DataTransferObject) {
                $value = $value->all();
            }

            if (is_array($value)) {
                $value = self::filter($value);

                continue;
            }

            if ($value !== null) {
                continue;
            }

            unset($data[$key]);
        }

        return $data;
    }
}
