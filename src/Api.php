<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi;

use Http\Client\Common\HttpMethodsClientInterface;

/**
 * @property HttpMethodsClientInterface $httpClient;
 */
interface Api
{
}
