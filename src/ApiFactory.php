<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi;

use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use ReflectionClass;
use ReflectionException;
use Wagento\GunBrokerApi\Exception\InvalidApiEndpointException;

use function class_exists;
use function count;
use function property_exists;

final class ApiFactory
{
    private ?StreamFactoryInterface $streamFactory;
    private ?RequestFactoryInterface $requestFactory;

    public function __construct(
        ?StreamFactoryInterface $streamFactory = null,
        ?RequestFactoryInterface $requestFactory = null
    ) {
        $this->streamFactory = $streamFactory;
        $this->requestFactory = $requestFactory;
    }

    /**
     * @throws InvalidApiEndpointException
     */
    public function create(string $apiEndpoint): Api
    {
        /** @var class-string<Api> $class */
        $class = "\Wagento\GunBrokerApi\Api\\{$apiEndpoint}";

        if (!class_exists($class)) {
            throw new InvalidApiEndpointException("Invalid API endpoint \"{$apiEndpoint}\".");
        }

        return $this->instantiateApiHandler($class);
    }

    /**
     * @param class-string<Api> $className
     */
    private function instantiateApiHandler(string $className): Api
    {
        try {
            $reflectionClass = new ReflectionClass($className);
        } catch (ReflectionException $e) {
            return new $className();
        }

        $constructor = $reflectionClass->getConstructor();

        if ($constructor === null) {
            return new $className();
        }

        $constructorArguments = $constructor->getParameters();

        if (count($constructorArguments) === 0) {
            return new $className();
        }

        $argumentsToInject = [];

        foreach ($constructorArguments as $constructorArgument) {
            $argumentName = $constructorArgument->getName();

            if (!property_exists($this, $argumentName)) {
                continue;
            }

            $argumentsToInject[] = $this->{$argumentName};
        }

        return new $className(...$argumentsToInject);
    }
}
