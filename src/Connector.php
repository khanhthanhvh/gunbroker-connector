<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi;

use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin\AuthenticationPlugin;
use Http\Client\Common\Plugin\BaseUriPlugin;
use Http\Client\Common\Plugin\LoggerPlugin;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Message\Formatter;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Log\LoggerInterface;
use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api\Users\AccessToken as AccessTokenApi;
use Wagento\GunBrokerApi\ApiObjects\Input\AddressBook as AddressBookRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\BankAccounts as BankAccountsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Categories as CategoriesRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\CreditRequests as CreditRequestsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FFLs as FFLSearchRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FFLs\Zip as FFLsByZipCodeRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FeedbackCreate as FeedbackCreateRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FeedbackSearch as UserFeedbackSearchRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FraudClaims\Buyer as BuyersFraudClaimsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FraudClaims\Seller as SellersFraudClaimsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Item as ListItemRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemSearch as ItemSearchRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Items as ItemsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Items\Showcase as ShowcaseItemListingsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsBidOn as ItemsBidOnRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsNotWon as ItemsNotWonRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsScheduled as ItemsScheduledRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsSelling as ItemsSellingRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsSold as ItemsSoldRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsUnsold as ItemsUnsoldRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsWatched as ItemsWatchedRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsWon as ItemsWonRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Orders\Flags as OrderFlagsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Orders\Shipping as OrderShippingDetailsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\OrdersSold as OrdersSoldRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\OrdersWon as OrdersWonRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Pictures as PicturesRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\SEOData as SEODataRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\SaveSearch as SaveSearchRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\SavedSearches as SavedSearchesRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Shipments as ShipmentHistoryRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ShippingProfile as ShippingProfileRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ShippingProfileByValue as ShippingProfileByValueRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ShippingProfileSearch as ShippingProfileSearchRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\UpdatedItem as UpdateItemRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Users\AccessToken as AccessTokenRequestData;
use Wagento\GunBrokerApi\ApiObjects\Input\Users\ContactInfo as ContactInfoRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\WatchItem as WatchItemRequest;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\AddressBook as AddressBookResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\BankAccounts as BankAccountsResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\BillingInformation as BillingInformationResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Categories as CategoriesResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Category as CategoryResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\CreditRequests as CreditRequestsResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\FFL as FFLDetailsResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\FFLs as FFLSearchResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Feedback\Summary as UserFeedbackSummaryResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\FeedbackSearch as UserFeedbackSearchResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\FraudClaims\Buyer as BuyersFraudClaimsResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\FraudClaims\Seller as SellersFraudClaimsResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\GunBrokerTime as GunBrokerTimeResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Item as ItemResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemBidOn as ItemBidOnResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemSearch as ItemSearchResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Items as ItemCollection;
use Wagento\GunBrokerApi\ApiObjects\Output\Items\Showcase as ShowcaseItemListingsResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsNotWon as ItemsNotWonResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsScheduled as ItemsScheduledResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsSelling as ItemsSellingResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsSold as ItemsSoldResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsUnsold as ItemsUnsoldResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsWatched as ItemsWatchedResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsWon as ItemsWonResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Order as OrderResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\OrdersSold as OrdersSoldResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\OrdersWon as OrdersWonResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Pictures as PicturesResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\SEOData as SEODataResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\SavedSearches as SavedSearchesResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Shipments as ShipmentHistoryResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ShippingProfile as ShippingProfileResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ShippingProfileSearch as ShippingProfileSearchResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ShippingProfileSearchResult as ShippingProfileSearchResultResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Users\AccountInfo as AccountInfoResponse;
use Wagento\GunBrokerApi\ApiObjects\UserInfo as UserInfoResponse;
use Wagento\GunBrokerApi\Exception\ConnectorException;
use Wagento\GunBrokerApi\Exception\InvalidApiEnvironmentException;
use Wagento\GunBrokerApi\Exception\InvalidConnectorMethodException;
use Wagento\GunBrokerApi\Exception\InvalidDevKeyException;
use Wagento\GunBrokerApi\HttpClient\Authentication\AccessToken as AccessTokenAuthentication;
use Wagento\GunBrokerApi\HttpClient\Factory as HttpClientFactory;

use function array_combine;
use function count;
use function uuid_is_valid;

/**
 * GunBroker.com API Connector
 *
 * See https://api.gunbroker.com/User/ for more details about the GunBroker.com API.
 *
 * @method AddressBookResponse getUserAddresses(AddressBookRequest $addressBookRequestData)
 * @method BankAccountsResponse getUserBankAccounts(BankAccountsRequest $bankAccountsRequestData)
 * @method BillingInformationResponse getUserBillingInformation()
 * @method CategoriesResponse getListingCategories(CategoriesRequest $categoriesRequestData)
 * @method CategoryResponse getListingCategory(int $categoryId)
 * @method CreditRequestsResponse getCreditRequests(CreditRequestsRequest $creditRequestsRequestData)
 * @method MessageResponse postItemFeedback(FeedbackCreateRequest $feedbackRequestData)
 * @method UserFeedbackSearchResponse getUserFeedback(int $userId, UserFeedbackSearchRequest $feedbackSearchRequestData)
 * @method UserFeedbackSummaryResponse getUserFeedbackSummary(int $userId)
 * @method FFLDetailsResponse getFFLDetails(int $fflId)
 * @method FFLDetailsResponse getFFLsByZipCode(int $zip, FFLsByZipCodeRequest $fflsByZipCodeRequestData)
 * @method FFLSearchResponse searchFFLs(FFLSearchRequest $fflSearchRequestData)
 * @method BuyersFraudClaimsResponse getBuyersFraudClaims(BuyersFraudClaimsRequest $buyersFraudClaimsRequestData)
 * @method SellersFraudClaimsResponse getSellersFraudClaims(SellersFraudClaimsRequest $buyersFraudClaimsRequestData)
 * @method GunBrokerTimeResponse getGunBrokerServerTime()
 * @method ItemSearchResponse searchListedItems(ItemSearchRequest $itemSearchRequestData)
 * @method MessageResponse listItem(ListItemRequest $itemRequestData, array $pictures)
 * @method MessageResponse updateListedItem(int $itemId, UpdateItemRequest $itemRequestData)
 * @method MessageResponse deleteListedItem(int $itemId)
 * @method ItemResponse getListedItem(int $itemId)
 * @method ItemCollection getListedItems(ItemsRequest $itemsRequestData)
 * @method ShowcaseItemListingsResponse getShowcaseItemListings(ShowcaseItemListingsRequest $showcaseItemsRequestData)
 * @method ItemBidOnResponse getItemsUserBidOn(ItemsBidOnRequest $itemsBidOnRequestData)
 * @method ItemsNotWonResponse getItemsNotWonByUser(ItemsNotWonRequest $itemsNotWonRequestData)
 * @method ItemsScheduledResponse getItemsScheduledByUser(ItemsScheduledRequest $itemsScheduledRequestData)
 * @method ItemsSellingResponse getActiveItems(ItemsSellingRequest $itemsSellingRequestData)
 * @method ItemsSoldResponse getItemsSoldByUser(ItemsSoldRequest $itemsSoldRequestData)
 * @method ItemsUnsoldResponse getItemsUnsoldByUser(ItemsUnsoldRequest $itemsUnsoldRequestData)
 * @method ItemsWatchedResponse getItemsWatchedByUser(ItemsWatchedRequest $itemsWatchedRequestData)
 * @method MessageResponse addItemToUsersWatchList(WatchItemRequest $watchItemRequestData)
 * @method bool removeItemFromUsersWatchList(int $itemId)
 * @method ItemsWonResponse getItemsWonByUser(ItemsWonRequest $itemsWonRequestData)
 * @method OrderResponse getOrder(int $orderId)
 * @method MessageResponse updateOrderFlags(int $orderId, OrderFlagsRequest $orderFlagsRequestData)
 * @method MessageResponse updateOrderShippingDetails(int $orderId, OrderShippingDetailsRequest $orderShippingRequestData)
 * @method OrdersSoldResponse getSoldOrders(OrdersSoldRequest $ordersSoldRequestData)
 * @method OrdersWonResponse getWonOrders(OrdersWonRequest $ordersWonRequestData)
 * @method PicturesResponse getItemPictures(PicturesRequest $picturesRequestData)
 * @method SavedSearchesResponse getSavedSearches(SavedSearchesRequest $savedSearchesRequestData)
 * @method MessageResponse saveSearch(SaveSearchRequest $saveSearchRequestData)
 * @method SEODataResponse getSeoData(SEODataRequest $seoDataRequestData)
 * @method ShipmentHistoryResponse getShipmentHistory(ShipmentHistoryRequest $shipmentHistoryRequestData)
 * @method ShippingProfileResponse getShippingProfile(ShippingProfileRequest $shippingProfileRequestData)
 * @method ShippingProfileSearchResultResponse getShippingProfileByValue(ShippingProfileByValueRequest $shippingProfileRequestData)
 * @method ShippingProfileSearchResponse findShippingProfile(ShippingProfileSearchRequest $shippingProfileSearchRequestData)
 * @method bool deleteAccessToken()
 * @method AccountInfoResponse getAccountDetails()
 * @method UserInfoResponse getContactDetails(ContactInfoRequest $contactInfoRequestData)
 */
class Connector implements ConnectorInterface
{
    private UriFactoryInterface $uriFactory;
    private HttpClientFactory $httpClientFactory;
    private ?ApiFactory $apiFactory;
    private ?RequestSenderInterface $requestSender = null;
    private ?HttpMethodsClientInterface $httpClient = null;
    private string $apiEnvironment = 'sandbox';
    private string $apiDevKey = '';
    private string $accessToken = '';

    public function __construct(
        ?UriFactoryInterface $uriFactory = null,
        ?HttpClientFactory $httpClientFactory = null,
        ?ApiFactory $apiFactory = null
    ) {
        $this->uriFactory = $uriFactory ?? Psr17FactoryDiscovery::findUriFactory();
        $this->httpClientFactory = $httpClientFactory ?? new HttpClientFactory();
        $this->apiFactory = $apiFactory;
    }

    /**
     * @param mixed[] $arguments
     * @return DataTransferObject|bool
     * @throws ConnectorException
     */
    public function __call(string $name, array $arguments = [])
    {
        $requestDetails = $this->getRequestDetailsByMethodName($name);
        $requestParameters = null;
        $queryParameters = [];

        if (count($arguments) > 0) {
            foreach ($arguments as $index => $argument) {
                if (!$argument instanceof DataTransferObject) {
                    continue;
                }

                $requestParameters = $argument;

                unset($arguments[$index]);

                break;
            }
        }

        if (count($arguments) > 0) {
            $queryParameters = array_combine($requestDetails['parameters'], $arguments) ?: [];
        }

        $response = $this->sendRequest(
            $requestDetails['endpoint'],
            $requestDetails['requestMethod'],
            $requestParameters,
            $queryParameters
        );

        return $response;
    }

    public function getHttpClient(): HttpMethodsClientInterface
    {
        if ($this->httpClient === null) {
            $this->configureHttpClient();

            $this->httpClient = $this->httpClientFactory->create();
        }

        return $this->httpClient;
    }

    public function setRequestSender(RequestSenderInterface $requestSender): ConnectorInterface
    {
        $this->requestSender = $requestSender;

        return $this;
    }

    public function getRequestSender(): RequestSenderInterface
    {
        if ($this->requestSender === null) {
            $this->requestSender = new RequestSender($this->getHttpClient(), $this->apiFactory);
        }

        return $this->requestSender;
    }

    public function setLogger(LoggerInterface $logger, Formatter $formatter = null): ConnectorInterface
    {
        $this->httpClientFactory->addPlugin(new LoggerPlugin($logger, $formatter));

        return $this;
    }

    /**
     * @throws ConnectorException
     */
    public function setApiEnvironment(string $apiEnvironment): ConnectorInterface
    {
        if ($apiEnvironment !== 'sandbox' && $apiEnvironment !== 'production') {
            throw new InvalidApiEnvironmentException();
        }

        $this->apiEnvironment = $apiEnvironment;

        return $this;
    }

    /**
     * @throws ConnectorException
     */
    public function setApiDevKey(string $devKey): ConnectorInterface
    {
        if (!uuid_is_valid($devKey)) {
            throw new InvalidDevKeyException();
        }

        $this->apiDevKey = $devKey;

        return $this;
    }

    public function setAccessToken(string $accessToken): ConnectorInterface
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @throws ConnectorException
     */
    public function configureAuthentication(
        string $apiUsername,
        string $apiPassword,
        ?string $accessToken = null
    ): ConnectorInterface {
        if ($accessToken === null && $this->accessToken === '') {
            $this->requestAccessToken($apiUsername, $apiPassword);

            $this->httpClient = null;
        }

        if ($accessToken !== null) {
            $this->accessToken = $accessToken;
        }

        $this->httpClientFactory->addPlugin(
            new AuthenticationPlugin(new AccessTokenAuthentication($this->accessToken))
        );

        return $this;
    }

    /**
     * @throws ConnectorException
     */
    public function requestAccessToken(string $username, string $password): string
    {
        $accessTokenRequestData = new AccessTokenRequestData();
        $accessTokenRequestData->username = $username;
        $accessTokenRequestData->password = $password;

        $accessTokenApi = new AccessTokenApi();
        $accessTokenApi->httpClient = $this->getHttpClient();

        $this->accessToken = $accessTokenApi->post($accessTokenRequestData)->accessToken;

        return $this->accessToken;
    }

    /**
     * @param mixed[] $queryParameters
     * @return DataTransferObject|bool
     * @throws ConnectorException
     */
    public function sendRequest(
        string $endpoint,
        string $requestMethod,
        ?DataTransferObject $requestParameters = null,
        array $queryParameters = []
    ) {
        return $this->getRequestSender()->sendRequest(
            $endpoint,
            $requestMethod,
            $requestParameters,
            $queryParameters
        );
    }

    private function configureHttpClient(): void
    {
        $apiUrl = $this->apiEnvironment === 'sandbox' ? self::BASE_SANDBOX_API_URL : self::BASE_PRODUCTION_API_URL;

        $this->httpClientFactory->addHttpHeader('X-DevKey', $this->apiDevKey);
        $this->httpClientFactory->addPlugin(new BaseUriPlugin($this->uriFactory->createUri($apiUrl)));
    }

    /**
     * @return array{endpoint:string, requestMethod:string, parameters:string[]}
     */
    private function getRequestDetailsByMethodName(string $name): array
    {
        $requestDetails = [
            'endpoint' => '',
            'requestMethod' => '',
            'parameters' => []
        ];

        switch ($name) {
            case 'getUserAddresses':
                $requestDetails['endpoint'] = 'AddressBook';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getUserBankAccounts':
                $requestDetails['endpoint'] = 'BankAccounts';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getUserBillingInformation':
                $requestDetails['endpoint'] = 'BillingInformation';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getListingCategories':
                $requestDetails['endpoint'] = 'Categories';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getListingCategory':
                $requestDetails['endpoint'] = 'Categories\CategoryID';
                $requestDetails['requestMethod'] = 'GET';
                $requestDetails['parameters'][] = 'categoryID';

                break;
            case 'getCreditRequests':
                $requestDetails['endpoint'] = 'CreditRequests';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'postItemFeedback':
                $requestDetails['endpoint'] = 'Feedback';
                $requestDetails['requestMethod'] = 'POST';

                break;
            case 'getUserFeedback':
                $requestDetails['endpoint'] = 'Feedback\UserID';
                $requestDetails['requestMethod'] = 'GET';
                $requestDetails['parameters'][] = 'userID';

                break;
            case 'getUserFeedbackSummary':
                $requestDetails['endpoint'] = 'Feedback\Summary';
                $requestDetails['requestMethod'] = 'GET';
                $requestDetails['parameters'][] = 'userID';

                break;
            case 'getFFLDetails':
                $requestDetails['endpoint'] = 'FFLs\FFLID';
                $requestDetails['requestMethod'] = 'GET';
                $requestDetails['parameters'][] = 'fflID';

                break;
            case 'getFFLsByZipCode':
                $requestDetails['endpoint'] = 'FFLs\Zip';
                $requestDetails['requestMethod'] = 'GET';
                $requestDetails['parameters'][] = 'zip';

                break;
            case 'searchFFLs':
                $requestDetails['endpoint'] = 'FFLs';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getBuyersFraudClaims':
                $requestDetails['endpoint'] = 'FraudClaims\Buyer';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getSellersFraudClaims':
                $requestDetails['endpoint'] = 'FraudClaims\Seller';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getGunBrokerServerTime':
                $requestDetails['endpoint'] = 'GunBrokerTime';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'searchListedItems':
                $requestDetails['endpoint'] = 'Items';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'listItem':
                $requestDetails['endpoint'] = 'Items';
                $requestDetails['requestMethod'] = 'POST';
                $requestDetails['parameters'][] = 'pictures';

                break;
            case 'updateListedItem':
                $requestDetails['endpoint'] = 'Items\ItemID';
                $requestDetails['requestMethod'] = 'PUT';
                $requestDetails['parameters'][] = 'itemID';

                break;
            case 'deleteListedItem':
                $requestDetails['endpoint'] = 'Items\ItemID';
                $requestDetails['requestMethod'] = 'DELETE';
                $requestDetails['parameters'][] = 'itemID';

                break;
            case 'getListedItem':
                $requestDetails['endpoint'] = 'Items\ItemID';
                $requestDetails['requestMethod'] = 'GET';
                $requestDetails['parameters'][] = 'itemID';

                break;
            case 'getListedItems':
                $requestDetails['endpoint'] = 'Items\ItemIDs';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getShowcaseItemListings':
                $requestDetails['endpoint'] = 'Items\Showcase';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getItemsUserBidOn':
                $requestDetails['endpoint'] = 'ItemsBidOn';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getItemsNotWonByUser':
                $requestDetails['endpoint'] = 'ItemsNotWon';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getItemsScheduledByUser':
                $requestDetails['endpoint'] = 'ItemsScheduled';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getActiveItems':
                $requestDetails['endpoint'] = 'ItemsSelling';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getItemsSoldByUser':
                $requestDetails['endpoint'] = 'ItemsSold';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getItemsUnsoldByUser':
                $requestDetails['endpoint'] = 'ItemsUnsold';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getItemsWatchedByUser':
                $requestDetails['endpoint'] = 'ItemsWatched';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'addItemToUsersWatchList':
                $requestDetails['endpoint'] = 'ItemsWatched';
                $requestDetails['requestMethod'] = 'POST';

                break;
            case 'removeItemFromUsersWatchList':
                $requestDetails['endpoint'] = 'ItemsWatched';
                $requestDetails['requestMethod'] = 'DELETE';
                $requestDetails['parameters'][] = 'itemID';

                break;
            case 'getItemsWonByUser':
                $requestDetails['endpoint'] = 'ItemsWon';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getOrder':
                $requestDetails['endpoint'] = 'Orders';
                $requestDetails['requestMethod'] = 'GET';
                $requestDetails['parameters'][] = 'orderId';

                break;
            case 'updateOrderFlags':
                $requestDetails['endpoint'] = 'Orders\Flags';
                $requestDetails['requestMethod'] = 'PUT';
                $requestDetails['parameters'][] = 'orderID';

                break;
            case 'updateOrderShippingDetails':
                $requestDetails['endpoint'] = 'Orders\Shipping';
                $requestDetails['requestMethod'] = 'PUT';
                $requestDetails['parameters'][] = 'orderID';

                break;
            case 'getSoldOrders':
                $requestDetails['endpoint'] = 'OrdersSold';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getWonOrders':
                $requestDetails['endpoint'] = 'OrdersWon';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getItemPictures':
                $requestDetails['endpoint'] = 'Pictures';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getSavedSearches':
                $requestDetails['endpoint'] = 'SavedSearches';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'saveSearch':
                $requestDetails['endpoint'] = 'SavedSearches';
                $requestDetails['requestMethod'] = 'POST';

                break;
            case 'getSeoData':
                $requestDetails['endpoint'] = 'SEOData';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getShipmentHistory':
                $requestDetails['endpoint'] = 'Shipments';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getShippingProfile':
                $requestDetails['endpoint'] = 'ShippingProfile';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getShippingProfileByValue':
                $requestDetails['endpoint'] = 'ShippingProfile\Value';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'findShippingProfile':
                $requestDetails['endpoint'] = 'ShippingProfileSearch';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'deleteAccessToken':
                $requestDetails['endpoint'] = 'Users\AccessToken';
                $requestDetails['requestMethod'] = 'DELETE';

                break;
            case 'getAccountDetails':
                $requestDetails['endpoint'] = 'Users\AccountInfo';
                $requestDetails['requestMethod'] = 'GET';

                break;
            case 'getContactDetails':
                $requestDetails['endpoint'] = 'Users\ContactInfo';
                $requestDetails['requestMethod'] = 'GET';

                break;
            default:
                throw new InvalidConnectorMethodException($name);
        }

        return $requestDetails;
    }
}
