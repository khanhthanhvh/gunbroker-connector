<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Exception;

use InvalidArgumentException;
use Throwable;

use function trim;

final class InvalidApiEnvironmentException extends InvalidArgumentException implements ConnectorException
{
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        if (trim($message) === '') {
            $message = 'API environment must be "sandbox" or "production". Please try again with the correct environment.';
        }

        parent::__construct($message, $code, $previous);
    }
}
