<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Exception;

use Throwable;

interface ConnectorException extends Throwable
{
}
