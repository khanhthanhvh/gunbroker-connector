<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Exception;

use RuntimeException;
use Throwable;
use Wagento\GunBrokerApi\Traits\ContainsMessageResponse;

use function trim;

final class GenericApiException extends RuntimeException implements ApiException
{
    use ContainsMessageResponse;

    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        if (trim($message) === '') {
            $message = 'A problem occurred while communicating with the GunBroker.com API. Please try your request again.';
        }

        parent::__construct($message, $code, $previous);
    }
}
