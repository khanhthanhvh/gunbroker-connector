<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Exception;

use InvalidArgumentException;
use Throwable;
use Wagento\GunBrokerApi\Traits\ContainsMessageResponse;

use function trim;

final class InvalidUsernameOrPasswordException extends InvalidArgumentException implements ApiException
{
    use ContainsMessageResponse;

    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        if (trim($message) === '') {
            $message = 'The provided username or password is not valid. Please verify your request and try again.';
        }

        parent::__construct($message, $code, $previous);
    }
}
