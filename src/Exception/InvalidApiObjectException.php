<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Exception;

use InvalidArgumentException;
use Throwable;

use function trim;

final class InvalidApiObjectException extends InvalidArgumentException implements ConnectorException
{
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        if (trim($message) === '') {
            $message = 'The provided API object is not valid for this API handler. Please verify that it is correct ' .
                'and try again.';
        }

        parent::__construct($message, $code, $previous);
    }
}
