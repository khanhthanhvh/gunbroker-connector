<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Exception;

use InvalidArgumentException;

final class InvalidConnectorMethodException extends InvalidArgumentException implements ConnectorException
{
    public function __construct(string $methodName)
    {
        $message = "The requested API connector method \"{$methodName}\" is not valid. Please verify that it is " .
            'correct and try again.';

        parent::__construct($message);
    }
}
