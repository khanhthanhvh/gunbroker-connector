<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Exception;

use RuntimeException;
use Throwable;

use function trim;

final class InvalidApiResponseException extends RuntimeException implements ConnectorException
{
    public static string $responseBody;

    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        if (trim($message) === '') {
            $message = 'The API response is not valid. Please try your request again later.';
        }

        parent::__construct($message, $code, $previous);
    }

    public static function withResponseBody(
        string $responseBody,
        string $message = '',
        int $code = 0,
        Throwable $previous = null
    ): InvalidApiResponseException {
        self::$responseBody = $responseBody;

        return new self($message, $code, $previous);
    }
}
