<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Exception;

use RuntimeException;
use Throwable;
use Wagento\GunBrokerApi\Traits\ContainsMessageResponse;

use function trim;

final class ExpiredAccessTokenException extends RuntimeException implements ApiException
{
    use ContainsMessageResponse;

    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        if (trim($message) === '') {
            $message = 'The provided access token has expired. Please request a new token.';
        }

        parent::__construct($message, $code, $previous);
    }
}
