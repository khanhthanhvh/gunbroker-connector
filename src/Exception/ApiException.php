<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Exception;

use Wagento\GunBrokerApi\ApiObjects\MessageResponse;

interface ApiException extends ConnectorException
{
    public function setMessageResponse(?MessageResponse $messageResponse): void;

    public function getMessageResponse(): ?MessageResponse;
}
