<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Exception;

use InvalidArgumentException;
use Throwable;
use Wagento\GunBrokerApi\Traits\ContainsMessageResponse;

use function trim;

final class MissingAccessTokenException extends InvalidArgumentException implements ApiException
{
    use ContainsMessageResponse;

    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        if (trim($message) === '') {
            $message = 'Please provide an access token with your request and try again.';
        }

        parent::__construct($message, $code, $previous);
    }
}
