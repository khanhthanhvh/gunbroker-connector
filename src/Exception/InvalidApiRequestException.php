<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Exception;

use RuntimeException;
use Throwable;

use function trim;

final class InvalidApiRequestException extends RuntimeException implements ConnectorException
{
    /**
     * @var mixed[]
     */
    public static array $requestData;

    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        if (trim($message) === '') {
            $message = 'The API request is not valid. Please check the request and try again.';
        }

        parent::__construct($message, $code, $previous);
    }

    /**
     * @param mixed[] $requestData
     */
    public static function withRequestData(
        array $requestData,
        string $message = '',
        int $code = 0,
        Throwable $previous = null
    ): InvalidApiRequestException {
        self::$requestData = $requestData;

        return new self($message, $code, $previous);
    }
}
