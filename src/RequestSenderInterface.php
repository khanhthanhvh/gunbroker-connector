<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Exception\ConnectorException;

interface RequestSenderInterface
{
    /**
     * @param mixed[] $additionalParameters
     * @return DataTransferObject|bool
     * @throws ConnectorException
     */
    public function sendRequest(
        string $apiEndpoint,
        string $httpMethod,
        ?DataTransferObject $requestParameters = null,
        array $additionalParameters = []
    );
}
