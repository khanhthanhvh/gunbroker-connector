<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi;

use Http\Client\Common\HttpMethodsClientInterface;
use Http\Message\Formatter;
use Psr\Log\LoggerInterface;
use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\Input\AddressBook as AddressBookRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\BankAccounts as BankAccountsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Categories as CategoriesRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\CreditRequests as CreditRequestsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FFLs as FFLSearchRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FFLs\Zip as FFLsByZipCodeRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FeedbackCreate as FeedbackCreateRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FeedbackSearch as UserFeedbackSearchRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FraudClaims\Buyer as BuyersFraudClaimsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FraudClaims\Seller as SellersFraudClaimsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Item as ListItemRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemSearch as ItemSearchRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Items as ItemsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Items\Showcase as ShowcaseItemListingsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsBidOn as ItemsBidOnRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsNotWon as ItemsNotWonRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsScheduled as ItemsScheduledRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsSelling as ItemsSellingRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsSold as ItemsSoldRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsUnsold as ItemsUnsoldRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsWatched as ItemsWatchedRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsWon as ItemsWonRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Orders\Flags as OrderFlagsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Orders\Shipping as OrderShippingDetailsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\OrdersSold as OrdersSoldRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\OrdersWon as OrdersWonRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Pictures as PicturesRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\SEOData as SEODataRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\SaveSearch as SaveSearchRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\SavedSearches as SavedSearchesRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Shipments as ShipmentHistoryRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ShippingProfile as ShippingProfileRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ShippingProfileByValue as ShippingProfileByValueRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ShippingProfileSearch as ShippingProfileSearchRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\UpdatedItem as UpdateItemRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Users\ContactInfo as ContactInfoRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\WatchItem as WatchItemRequest;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\AddressBook as AddressBookResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\BankAccounts as BankAccountsResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\BillingInformation as BillingInformationResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Categories as CategoriesResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Category as CategoryResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\CreditRequests as CreditRequestsResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\FFL as FFLDetailsResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\FFLs as FFLSearchResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Feedback\Summary as UserFeedbackSummaryResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\FeedbackSearch as UserFeedbackSearchResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\FraudClaims\Buyer as BuyersFraudClaimsResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\FraudClaims\Seller as SellersFraudClaimsResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\GunBrokerTime as GunBrokerTimeResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Item as ItemResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemBidOn as ItemBidOnResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemSearch as ItemSearchResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Items as ItemCollection;
use Wagento\GunBrokerApi\ApiObjects\Output\Items\Showcase as ShowcaseItemListingsResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsNotWon as ItemsNotWonResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsScheduled as ItemsScheduledResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsSelling as ItemsSellingResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsSold as ItemsSoldResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsUnsold as ItemsUnsoldResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsWatched as ItemsWatchedResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsWon as ItemsWonResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Order as OrderResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\OrdersSold as OrdersSoldResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\OrdersWon as OrdersWonResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Pictures as PicturesResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\SEOData as SEODataResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\SavedSearches as SavedSearchesResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Shipments as ShipmentHistoryResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ShippingProfile as ShippingProfileResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ShippingProfileSearch as ShippingProfileSearchResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ShippingProfileSearchResult as ShippingProfileSearchResultResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Users\AccountInfo as AccountInfoResponse;
use Wagento\GunBrokerApi\ApiObjects\UserInfo as UserInfoResponse;
use Wagento\GunBrokerApi\Exception\ConnectorException;

/**
 * GunBroker.com API Connector
 *
 * See https://api.gunbroker.com/User/ for more details about the GunBroker.com API.
 *
 * @method AddressBookResponse getUserAddresses(AddressBookRequest $addressBookRequestData)
 * @method BankAccountsResponse getUserBankAccounts(BankAccountsRequest $bankAccountsRequestData)
 * @method BillingInformationResponse getUserBillingInformation()
 * @method CategoriesResponse getListingCategories(CategoriesRequest $categoriesRequestData)
 * @method CategoryResponse getListingCategory(int $categoryId)
 * @method CreditRequestsResponse getCreditRequests(CreditRequestsRequest $creditRequestsRequestData)
 * @method MessageResponse postItemFeedback(FeedbackCreateRequest $feedbackRequestData)
 * @method UserFeedbackSearchResponse getUserFeedback(int $userId, UserFeedbackSearchRequest $feedbackSearchRequestData)
 * @method UserFeedbackSummaryResponse getUserFeedbackSummary(int $userId)
 * @method FFLDetailsResponse getFFLDetails(int $fflId)
 * @method FFLDetailsResponse getFFLsByZipCode(int $zip, FFLsByZipCodeRequest $fflsByZipCodeRequestData)
 * @method FFLSearchResponse searchFFLs(FFLSearchRequest $fflSearchRequestData)
 * @method BuyersFraudClaimsResponse getBuyersFraudClaims(BuyersFraudClaimsRequest $buyersFraudClaimsRequestData)
 * @method SellersFraudClaimsResponse getSellersFraudClaims(SellersFraudClaimsRequest $buyersFraudClaimsRequestData)
 * @method GunBrokerTimeResponse getGunBrokerServerTime()
 * @method ItemSearchResponse searchListedItems(ItemSearchRequest $itemSearchRequestData)
 * @method MessageResponse listItem(ListItemRequest $itemRequestData, array $pictures)
 * @method MessageResponse updateListedItem(int $itemId, UpdateItemRequest $itemRequestData)
 * @method MessageResponse deleteListedItem(int $itemId)
 * @method ItemResponse getListedItem(int $itemId)
 * @method ItemCollection getListedItems(ItemsRequest $itemsRequestData)
 * @method ShowcaseItemListingsResponse getShowcaseItemListings(ShowcaseItemListingsRequest $showcaseItemsRequestData)
 * @method ItemBidOnResponse getItemsUserBidOn(ItemsBidOnRequest $itemsBidOnRequestData)
 * @method ItemsNotWonResponse getItemsNotWonByUser(ItemsNotWonRequest $itemsNotWonRequestData)
 * @method ItemsScheduledResponse getItemsScheduledByUser(ItemsScheduledRequest $itemsScheduledRequestData)
 * @method ItemsSellingResponse getActiveItems(ItemsSellingRequest $itemsSellingRequestData)
 * @method ItemsSoldResponse getItemsSoldByUser(ItemsSoldRequest $itemsSoldRequestData)
 * @method ItemsUnsoldResponse getItemsUnsoldByUser(ItemsUnsoldRequest $itemsUnsoldRequestData)
 * @method ItemsWatchedResponse getItemsWatchedByUser(ItemsWatchedRequest $itemsWatchedRequestData)
 * @method MessageResponse addItemToUsersWatchList(WatchItemRequest $watchItemRequestData)
 * @method bool removeItemFromUsersWatchList(int $itemId)
 * @method ItemsWonResponse getItemsWonByUser(ItemsWonRequest $itemsWonRequestData)
 * @method OrderResponse getOrder(int $orderId)
 * @method MessageResponse updateOrderFlags(int $orderId, OrderFlagsRequest $orderFlagsRequestData)
 * @method MessageResponse updateOrderShippingDetails(int $orderId, OrderShippingDetailsRequest $orderShippingRequestData)
 * @method OrdersSoldResponse getSoldOrders(OrdersSoldRequest $ordersSoldRequestData)
 * @method OrdersWonResponse getWonOrders(OrdersWonRequest $ordersWonRequestData)
 * @method PicturesResponse getItemPictures(PicturesRequest $picturesRequestData)
 * @method SavedSearchesResponse getSavedSearches(SavedSearchesRequest $savedSearchesRequestData)
 * @method MessageResponse saveSearch(SaveSearchRequest $saveSearchRequestData)
 * @method SEODataResponse getSeoData(SEODataRequest $seoDataRequestData)
 * @method ShipmentHistoryResponse getShipmentHistory(ShipmentHistoryRequest $shipmentHistoryRequestData)
 * @method ShippingProfileResponse getShippingProfile(ShippingProfileRequest $shippingProfileRequestData)
 * @method ShippingProfileSearchResultResponse getShippingProfileByValue(ShippingProfileByValueRequest $shippingProfileRequestData)
 * @method ShippingProfileSearchResponse findShippingProfile(ShippingProfileSearchRequest $shippingProfileSearchRequestData)
 * @method bool deleteAccessToken()
 * @method AccountInfoResponse getAccountDetails()
 * @method UserInfoResponse getContactDetails(ContactInfoRequest $contactInfoRequestData)
 */
interface ConnectorInterface
{
    public const BASE_SANDBOX_API_URL = 'https://api.sandbox.gunbroker.com/v1/';
    public const BASE_PRODUCTION_API_URL = 'https://api.gunbroker.com/v1/';

    public function getHttpClient(): HttpMethodsClientInterface;

    public function setRequestSender(RequestSenderInterface $requestInitiator): ConnectorInterface;

    public function getRequestSender(): RequestSenderInterface;

    public function setLogger(LoggerInterface $logger, Formatter $formatter = null): ConnectorInterface;

    /**
     * @throws ConnectorException
     */
    public function setApiEnvironment(string $apiEnvironment): ConnectorInterface;

    /**
     * @throws ConnectorException
     */
    public function setApiDevKey(string $devKey): ConnectorInterface;

    public function setAccessToken(string $accessToken): ConnectorInterface;

    public function getAccessToken(): string;

    /**
     * @throws ConnectorException
     */
    public function configureAuthentication(
        string $apiUsername,
        string $apiPassword,
        ?string $accessToken = null
    ): ConnectorInterface;

    /**
     * @throws ConnectorException
     */
    public function requestAccessToken(string $username, string $password): string;

    /**
     * @param mixed[] $queryParameters
     * @return DataTransferObject|bool
     * @throws ConnectorException
     */
    public function sendRequest(
        string $endpoint,
        string $requestMethod,
        ?DataTransferObject $requestParameters = null,
        array $queryParameters = []
    );
}
