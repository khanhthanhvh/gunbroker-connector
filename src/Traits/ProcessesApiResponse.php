<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Traits;

use Psr\Http\Message\ResponseInterface;
use Wagento\GunBrokerApi\HttpClient\Message\ResponseProcessor;
use Wagento\GunBrokerApi\HttpClient\Message\ResponseProcessorInterface;

trait ProcessesApiResponse
{
    private ?ResponseProcessorInterface $responseProcessor;

    /**
     * @param mixed ...$arguments
     */
    public function withResponseProcessor(ResponseProcessorInterface $responseProcessor, ...$arguments): self
    {
        $this->responseProcessor = $responseProcessor;

        return new self(...$arguments);
    }

    /**
     * @return mixed[]
     */
    private function processResponse(ResponseInterface $response): array
    {
        $responseProcessor = $this->responseProcessor ?? new ResponseProcessor();

        return $responseProcessor->processResponse($response);
    }
}
