<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Traits;

use Psr\Log\LoggerInterface;

trait WritesLog
{
    private ?LoggerInterface $logger;

    public function setLogger(LoggerInterface $logger): self
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * @param mixed[] $context
     */
    private function log(string $level, string $message, array $context = []): void
    {
        if (empty($this->logger)) {
            return;
        }

        $this->logger->log($level, $message, $context);
    }
}
