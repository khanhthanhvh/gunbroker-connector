<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Traits;

use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\Exception\ConnectorException;

trait SendsApiDeleteRequest
{
    use ProcessesApiRequest;
    use SendsApiRequest;

    /**
     * @throws ConnectorException
     */
    public function delete(): MessageResponse
    {
        $responseData = $this->processResponse($this->httpClient->delete($this->getUri()));

        return new MessageResponse($responseData);
    }
}
