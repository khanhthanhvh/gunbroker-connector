<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Traits;

use Wagento\GunBrokerApi\HttpClient\Message\RequestProcessor;
use Wagento\GunBrokerApi\HttpClient\Message\RequestProcessorInterface;

trait ProcessesApiRequest
{
    private ?RequestProcessorInterface $requestProcessor;

    /**
     * @param mixed ...$arguments
     */
    public function withRequestProcessor(RequestProcessorInterface $requestProcessor, ...$arguments): self
    {
        $this->requestProcessor = $requestProcessor;

        return new self(...$arguments);
    }

    /**
     * @param mixed[] $requestData
     */
    private function processRequest(array $requestData, bool $allowNullValues = false): string
    {
        $requestProcessor = $this->requestProcessor ?? new RequestProcessor();

        return $requestProcessor->processRequest($requestData, $allowNullValues);
    }
}
