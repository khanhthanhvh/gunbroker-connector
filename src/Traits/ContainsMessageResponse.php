<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Traits;

use Wagento\GunBrokerApi\ApiObjects\MessageResponse;

trait ContainsMessageResponse
{
    private ?MessageResponse $messageResponse;

    public function setMessageResponse(?MessageResponse $messageResponse): void
    {
        $this->messageResponse = $messageResponse;
    }

    public function getMessageResponse(): ?MessageResponse
    {
        if (empty($this->messageResponse)) {
            return null;
        }

        return $this->messageResponse;
    }
}
