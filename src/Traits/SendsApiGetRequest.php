<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Traits;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Exception\ConnectorException;

use function http_build_query;

use const PHP_QUERY_RFC3986;

trait SendsApiGetRequest
{
    use SendsApiRequest;

    public string $apiResponseClass;

    /**
     * @throws ConnectorException
     *
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint
     */
    public function get(?DataTransferObject $requestData = null)
    {
        $uri = $this->getUri();

        if ($requestData !== null) {
            $this->validateInputObject($requestData, 'GET');

            $uri .= '?' . http_build_query($requestData, '', '&', PHP_QUERY_RFC3986);
        }

        $apiResponseClass = $this->getOutputObjectClass('GET');

        return new $apiResponseClass($this->processResponse($this->httpClient->get($uri)));
    }
}
