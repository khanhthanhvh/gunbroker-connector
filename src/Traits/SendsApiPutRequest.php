<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Traits;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\Exception\ConnectorException;

trait SendsApiPutRequest
{
    use ProcessesApiRequest;
    use SendsApiRequest;

    /**
     * @throws ConnectorException
     */
    public function put(DataTransferObject $requestData): DataTransferObject
    {
        $this->validateInputObject($requestData, 'PUT');

        $requestBody = $this->processRequest($requestData->all());
        $responseData = $this->processResponse($this->httpClient->put($this->getUri(), [], $requestBody));

        return new MessageResponse($responseData);
    }
}
