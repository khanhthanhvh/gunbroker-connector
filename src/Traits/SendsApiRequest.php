<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Traits;

use Http\Client\Common\HttpMethodsClientInterface;

use function array_walk;
use function count;
use function preg_match_all;
use function property_exists;
use function rtrim;
use function str_replace;

use const PREG_SET_ORDER;

trait SendsApiRequest
{
    use ContainsApiObjects;
    use ProcessesApiResponse;

    public HttpMethodsClientInterface $httpClient;

    private function getUri(): string
    {
        $uri = self::URI;

        preg_match_all('/{([A-Za-z\\\\]+)}/', $uri, $matches, PREG_SET_ORDER);

        if (count($matches) === 0) {
            return $uri;
        }

        array_walk(
            $matches,
            /** @param string[] $match */
            function (array $match) use (&$uri): void {
                $replacement = property_exists($this, $match[1]) ? $this->{$match[1]} : '';
                $uri = str_replace("{{$match[1]}}", $replacement, $uri);
            }
        );

        $uri = rtrim($uri, '/');

        return $uri;
    }
}
