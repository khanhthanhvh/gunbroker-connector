<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Traits;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\Exception\ConnectorException;

trait SendsApiPostRequest
{
    use ProcessesApiRequest;
    use SendsApiRequest;

    /**
     * @throws ConnectorException
     */
    public function post(DataTransferObject $requestData): DataTransferObject
    {
        $this->validateInputObject($requestData, 'POST');

        $requestBody = $this->processRequest($requestData->all());
        $responseData = $this->processResponse($this->httpClient->post($this->getUri(), [], $requestBody));

        return new MessageResponse($responseData);
    }
}
