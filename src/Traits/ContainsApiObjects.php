<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Traits;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Exception\InvalidApiObjectException;

use function array_key_exists;
use function get_class;
use function ltrim;

trait ContainsApiObjects
{
    private function validateInputObject(DataTransferObject $inputObject, string $httpMethod): void
    {
        $inputObjectClass = get_class($inputObject);

        if (
            !array_key_exists($httpMethod, $this->inputObjects)
            || ltrim($this->inputObjects[$httpMethod], '\\') !== $inputObjectClass
        ) {
            throw new InvalidApiObjectException(
                "The API input object provided \"{$inputObjectClass}\" is not valid for " . self::class . '.'
            );
        }
    }

    private function getOutputObjectClass(string $httpMethod): string
    {
        if (!array_key_exists($httpMethod, $this->outputObjects)) {
            throw new InvalidApiObjectException(
                self::class . " does not have an output object defined for the HTTP method \"{$httpMethod}\"."
            );
        }

        return $this->outputObjects[$httpMethod];
    }
}
