<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsScheduled as ItemsScheduledRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsScheduled as ItemsScheduledResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ItemsScheduled
 * @implements Get<ItemsScheduledRequestData, ItemsScheduledResponseData>
 * @method ItemsScheduledResponseData get(ItemsScheduledRequestData $itemsScheduledRequestData)
 */
final class ItemsScheduled implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/ItemsScheduled';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ItemsScheduledRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ItemsScheduledResponseData::class
    ];
}
