<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\FFLs;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Output\FFL as FFLResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/FFLsID
 * @implements Get<DataTransferObject, FFLResponseData>
 * @method FFLResponseData get()
 */
final class FFLID implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/FFLs/{fflID}';

    public int $fflID = 0;
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => FFLResponseData::class
    ];
}
