<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\FFLs;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\FFLs\Zip as FFLsZipRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\FFL as FFLsZipResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/FFLsZip
 * @implements Get<FFLsZipRequestData, FFLsZipResponseData>
 * @method FFLsZipResponseData get(FFLsZipRequestData $fflsZipRequestData)
 */
final class Zip implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/FFLs/Zip/{zip}';

    public string $zip = '';
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => FFLsZipRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => FFLsZipResponseData::class
    ];
}
