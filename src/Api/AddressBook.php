<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\AddressBook as AddressBookRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\AddressBook as AddressBookResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/AddressBookGet
 * @implements Get<AddressBookRequestData, AddressBookResponseData>
 * @method AddressBookResponseData get(AddressBookRequestData $addressBookRequestData)
 */
final class AddressBook implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/AddressBook';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => AddressBookRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => AddressBookResponseData::class
    ];
}
