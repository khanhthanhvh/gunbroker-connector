<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\FraudClaims;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\FraudClaims\Buyer as FraudClaimsBuyerRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\FraudClaims\Buyer as FraudClaimsBuyerResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/FraudClaimsBuyerGet
 * @implements Get<FraudClaimsBuyerRequestData, FraudClaimsBuyerResponseData>
 * @method FraudClaimsBuyerResponseData get(FraudClaimsBuyerRequestData $fraudClaimsBuyerRequestData)
 */
final class Buyer implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/FraudClaims/Buyer';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => FraudClaimsBuyerRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => FraudClaimsBuyerResponseData::class
    ];
}
