<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\FraudClaims;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\FraudClaims\Seller as FraudClaimsSellerRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\FraudClaims\Seller as FraudClaimsSellerResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/FraudClaimsSellerGet
 * @implements Get<FraudClaimsSellerRequestData, FraudClaimsSellerResponseData>
 * @method FraudClaimsSellerResponseData get(FraudClaimsSellerRequestData $fraudClaimsSellerRequestData)
 */
final class Seller implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/FraudClaims/Seller';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => FraudClaimsSellerRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => FraudClaimsSellerResponseData::class
    ];
}
