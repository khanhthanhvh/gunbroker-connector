<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\Pictures as PicturesRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\Pictures as PicturesResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/PicturesGet
 * @implements Get<PicturesRequestData, PicturesResponseData>
 * @method PicturesResponseData get(PicturesRequestData $picturesRequestData)
 */
final class Pictures implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/Pictures';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => PicturesRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => PicturesResponseData::class
    ];
}
