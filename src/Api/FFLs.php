<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\FFLs as FFLsRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\FFLs as FFLsResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/FFLs
 * @implements Get<FFLsRequestData, FFLsResponseData>
 * @method FFLsResponseData get(FFLsRequestData $fflsRequestData)
 */
final class FFLs implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/FFLs';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => FFLsRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => FFLsResponseData::class
    ];
}
