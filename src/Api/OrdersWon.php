<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\OrdersWon as OrdersWonRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\OrdersWon as OrdersWonResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/OrdersWon
 * @implements Get<OrdersWonRequestData, OrdersWonResponseData>
 * @method OrdersWonResponseData get(OrdersWonRequestData $ordersWonRequestData)
 */
final class OrdersWon implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/OrdersWon';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => OrdersWonRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => OrdersWonResponseData::class
    ];
}
