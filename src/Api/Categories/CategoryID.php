<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\Categories;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Output\Category as CategoryResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/CategoriesID
 * @implements Get<DataTransferObject, CategoryResponseData>
 * @method CategoryResponseData get()
 */
final class CategoryID implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/Categories/{categoryID}';

    public int $categoryID = 0;
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => CategoryResponseData::class
    ];
}
