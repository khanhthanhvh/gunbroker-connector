<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsWatched as ItemsWatchedRequestData;
use Wagento\GunBrokerApi\ApiObjects\Input\WatchItem as WatchItemRequestData;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsWatched as ItemsWatchedResponseData;
use Wagento\GunBrokerApi\HttpMethod\Delete;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\HttpMethod\Post;
use Wagento\GunBrokerApi\Traits\SendsApiDeleteRequest;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;
use Wagento\GunBrokerApi\Traits\SendsApiPostRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ItemsWatchedGet
 * @see https://api.gunbroker.com/User/Help/ItemsWatchedPost
 * @see https://api.gunbroker.com/User/Help/ItemsWatchedDelete
 * @implements Get<ItemsWatchedRequestData, ItemsWatchedResponseData>
 * @implements Post<WatchItemRequestData, MessageResponse>
 * @method ItemsWatchedResponseData get(ItemsWatchedRequestData $itemsWatchedRequestData)
 * @method MessageResponse post(WatchItemRequestData $watchItemRequestData)
 */
final class ItemsWatched implements Api, Get, Post, Delete
{
    use SendsApiGetRequest;
    use SendsApiPostRequest;
    use SendsApiDeleteRequest;

    private const URI = '/ItemsWatched';

    public int $itemID = 0;
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ItemsWatchedRequestData::class,
        'POST' => WatchItemRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ItemsWatchedResponseData::class
    ];
}
