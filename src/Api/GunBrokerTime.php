<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Output\GunBrokerTime as GunBrokerTimeResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/GunBrokerTime
 * @implements Get<DataTransferObject, GunBrokerTimeResponseData>
 * @method GunBrokerTimeResponseData get()
 */
final class GunBrokerTime implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/GunBrokerTime';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => GunBrokerTimeResponseData::class
    ];
}
