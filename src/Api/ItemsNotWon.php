<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsNotWon as ItemsNotWonRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsNotWon as ItemsNotWonResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ItemsNotWon
 * @implements Get<ItemsNotWonRequestData, ItemsNotWonResponseData>
 * @method ItemsNotWonResponseData get(ItemsNotWonRequestData $itemsNotWonRequestData)
 */
final class ItemsNotWon implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/ItemsNotWon';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ItemsNotWonRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ItemsNotWonResponseData::class
    ];
}
