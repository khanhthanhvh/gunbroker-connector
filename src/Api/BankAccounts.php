<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\BankAccounts as BankAccountsRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\BankAccounts as BankAccountsResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/BankAccountsGET
 * @implements Get<BankAccountsRequestData, BankAccountsResponseData>
 * @method BankAccountsResponseData get(BankAccountsRequestData $bankAccountsRequestData)
 */
final class BankAccounts implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/BankAccounts';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => BankAccountsRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => BankAccountsResponseData::class
    ];
}
