<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\Categories as CategoriesRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\Categories as CategoriesResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/Categories
 * @implements Get<CategoriesRequestData, CategoriesResponseData>
 * @method CategoriesResponseData get(CategoriesRequestData $categoriesRequestData)
 */
final class Categories implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/Categories';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => CategoriesRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => CategoriesResponseData::class
    ];
}
