<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsSelling as ItemsSellingRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsSelling as ItemsSellingResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ItemsSellingGet
 * @implements Get<ItemsSellingRequestData, ItemsSellingResponseData>
 * @method ItemsSellingResponseData get(ItemsSellingRequestData $itemsSellingRequestData)
 */
final class ItemsSelling implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/ItemsSelling';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ItemsSellingRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ItemsSellingResponseData::class
    ];
}
