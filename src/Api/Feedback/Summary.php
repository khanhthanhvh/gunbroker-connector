<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\Feedback;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Output\Feedback\Summary as FeedbackSummaryResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/FeedbackSummary
 * @implements Get<DataTransferObject, FeedbackSummaryResponseData>
 * @method FeedbackSummaryResponseData get()
 */
final class Summary implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/Feedback/Summary/{userID}';

    public int $userID = 0;
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => FeedbackSummaryResponseData::class
    ];
}
