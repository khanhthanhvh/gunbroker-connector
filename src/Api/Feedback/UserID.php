<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\Feedback;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\FeedbackSearch as FeedbackSearchRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\FeedbackSearch as FeedbackSearchResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/Feedback
 * @implements Get<FeedbackSearchRequestData, FeedbackSearchResponseData>
 * @method FeedbackSearchResponseData get(FeedbackSearchRequestData $feedbackSearchRequestData)
 */
final class UserID implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/Feedback/{userID}';

    public int $userID = 0;
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => FeedbackSearchRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => FeedbackSearchResponseData::class
    ];
}
