<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\ShippingProfile as ShippingProfileRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\ShippingProfile as ShippingProfileResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ShippingProfileTypeGet
 * @implements Get<ShippingProfileRequestData, ShippingProfileResponseData>
 * @method ShippingProfileResponseData get(ShippingProfileRequestData $shippingProfileRequestData)
 */
final class ShippingProfile implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/ShippingProfile';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ShippingProfileRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ShippingProfileResponseData::class
    ];
}
