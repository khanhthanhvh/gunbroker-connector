<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsWon as ItemsWonRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsWon as ItemsWonResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ItemsWon
 * @implements Get<ItemsWonRequestData, ItemsWonResponseData>
 * @method ItemsWonResponseData get(ItemsWonRequestData $itemsWonRequestData)
 */
final class ItemsWon implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/ItemsWon';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ItemsWonRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ItemsWonResponseData::class
    ];
}
