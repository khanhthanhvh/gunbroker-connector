<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsBidOn as ItemsBidOnRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsBidOn as ItemsBidOnResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ItemsBidOn
 * @implements Get<ItemsBidOnRequestData, ItemsBidOnResponseData>
 * @method ItemsBidOnResponseData get(ItemsBidOnRequestData $itemsBidOnRequestData)
 */
final class ItemsBidOn implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/ItemsBidOn';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ItemsBidOnRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ItemsBidOnResponseData::class
    ];
}
