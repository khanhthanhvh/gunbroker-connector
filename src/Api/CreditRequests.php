<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\CreditRequests as CreditRequestsRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\CreditRequests as CreditRequestsResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/CreditRequestsGet
 * @implements Get<CreditRequestsRequestData, CreditRequestsResponseData>
 * @method CreditRequestsResponseData get(CreditRequestsRequestData $creditRequestsRequestData)
 */
final class CreditRequests implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/CreditRequests';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => CreditRequestsRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => CreditRequestsResponseData::class
    ];
}
