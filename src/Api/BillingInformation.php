<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Output\BillingInformation as BillingInformationResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/BillingInformationGet
 * @implements Get<DataTransferObject, BillingInformationResponseData>
 * @method BillingInformationResponseData get()
 */
final class BillingInformation implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/BillingInformation';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => BillingInformationResponseData::class
    ];
}
