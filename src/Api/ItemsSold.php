<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsSold as ItemsSoldRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsSold as ItemsSoldResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ItemsSold
 * @implements Get<ItemsSoldRequestData, ItemsSoldResponseData>
 * @method ItemsSoldResponseData get(ItemsSoldRequestData $itemsSoldRequestData)
 */
final class ItemsSold implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/ItemsSold';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ItemsSoldRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ItemsSoldResponseData::class
    ];
}
