<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\FeedbackCreate as FeedbackRequestData;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\HttpMethod\Post;
use Wagento\GunBrokerApi\Traits\SendsApiPostRequest;

/**
 * @see https://api.gunbroker.com/User/Help/Feedback
 * @implements Post<FeedbackRequestData, MessageResponse>
 * @method MessageResponse post(FeedbackRequestData $feedbackRequestData)
 */
final class Feedback implements Api, Post
{
    use SendsApiPostRequest;

    private const URI = '/Feedback';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'POST' => FeedbackRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [];
}
