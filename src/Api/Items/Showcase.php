<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\Items;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\Items\Showcase as ItemsShowcaseRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\Items\Showcase as ItemsShowcaseResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ItemsShowcase
 * @implements Get<ItemsShowcaseRequestData, ItemsShowcaseResponseData>
 * @method ItemsShowcaseResponseData get(ItemsShowcaseRequestData $itemsShowcaseRequestData)
 */
final class Showcase implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/Items/Showcase';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ItemsShowcaseRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ItemsShowcaseResponseData::class
    ];
}
