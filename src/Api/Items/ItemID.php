<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\Items;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\UpdatedItem as ItemUpdateRequestData;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\Item as ItemResponseData;
use Wagento\GunBrokerApi\HttpMethod\Delete;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\HttpMethod\Put;
use Wagento\GunBrokerApi\Traits\SendsApiDeleteRequest;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;
use Wagento\GunBrokerApi\Traits\SendsApiPutRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ItemsID
 * @see https://api.gunbroker.com/User/Help/ItemsPut
 * @implements Get<DataTransferObject, ItemResponseData>
 * @implements Put<ItemUpdateRequestData, MessageResponse>
 * @method ItemResponseData get()
 * @method MessageResponse put(ItemUpdateRequestData $itemUpdateRequestData)
 */
final class ItemID implements Api, Get, Put, Delete
{
    use SendsApiGetRequest;
    use SendsApiPutRequest;
    use SendsApiDeleteRequest;

    private const URI = '/Items/{itemID}';

    public int $itemID = 0;
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'PUT' => ItemUpdateRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ItemResponseData::class
    ];
}
