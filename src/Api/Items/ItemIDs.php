<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\Items;

use Spatie\DataTransferObject\DataTransferObject;
use Spatie\DataTransferObject\DataTransferObjectCollection;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\Items as ItemsRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\Items as ItemsResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ItemsItemIDs
 * @implements Get<ItemsRequestData, ItemsResponseData>
 * @method ItemsResponseData get(ItemsRequestData $itemsRequestData)
 */
final class ItemIDs implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/Items';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ItemsRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObjectCollection>>
     */
    private array $outputObjects = [
        'GET' => ItemsResponseData::class
    ];
}
