<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\Users;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Output\Users\AccountInfo as AccountInfoResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/AccountInfoGet
 * @implements Get<DataTransferObject, AccountInfoResponseData>
 * @method AccountInfoResponseData get()
 */
final class AccountInfo implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/Users/AccountInfo';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => AccountInfoResponseData::class
    ];
}
