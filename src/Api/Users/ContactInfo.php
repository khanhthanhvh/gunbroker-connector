<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\Users;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\Users\ContactInfo as ContactInfoRequestData;
use Wagento\GunBrokerApi\ApiObjects\UserInfo as ContactInfoResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ContactInfoGet
 * @implements Get<ContactInfoRequestData, ContactInfoResponseData>
 * @method ContactInfoResponseData get(ContactInfoRequestData $contactInfoRequestData)
 */
final class ContactInfo implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/Users/ContactInfo';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ContactInfoRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ContactInfoResponseData::class
    ];
}
