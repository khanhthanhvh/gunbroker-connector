<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\Users;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\Users\AccessToken as AccessTokenRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\Users\AccessToken as AccessTokenResponseData;
use Wagento\GunBrokerApi\Exception\ConnectorException;
use Wagento\GunBrokerApi\HttpMethod\Delete;
use Wagento\GunBrokerApi\HttpMethod\Post;
use Wagento\GunBrokerApi\Traits\ProcessesApiRequest;
use Wagento\GunBrokerApi\Traits\ProcessesApiResponse;
use Wagento\GunBrokerApi\Traits\SendsApiDeleteRequest;
use Wagento\GunBrokerApi\Traits\SendsApiRequest;

/**
 * @see https://api.gunbroker.com/User/Help/AccessTokenPost
 * @see https://api.gunbroker.com/User/Help/AccessTokenDelete
 * @implements Post<AccessTokenRequestData, AccessTokenResponseData>
 */
final class AccessToken implements Api, Post, Delete
{
    use ProcessesApiRequest;
    use ProcessesApiResponse;
    use SendsApiRequest;
    use SendsApiDeleteRequest;

    private const URI = '/Users/AccessToken';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'POST' => AccessTokenRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'POST' => AccessTokenResponseData::class
    ];

    /**
     * @param AccessTokenRequestData $requestData
     * @return AccessTokenResponseData
     * @throws ConnectorException
     */
    public function post(DataTransferObject $requestData): DataTransferObject
    {
        $this->validateInputObject($requestData, 'POST');

        $requestBody = $this->processRequest($requestData->all());
        $responseData = $this->processResponse($this->httpClient->post(self::URI, [], $requestBody));

        return new AccessTokenResponseData($responseData);
    }
}
