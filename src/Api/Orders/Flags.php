<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\Orders;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\Orders\Flags as OrderFlagsRequestData;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\HttpMethod\Put;
use Wagento\GunBrokerApi\Traits\SendsApiPutRequest;

/**
 * @see https://api.gunbroker.com/User/Help/OrdersFlagsPut
 * @implements Put<OrderFlagsRequestData, MessageResponse>
 * @method MessageResponse put(OrderFlagsRequestData $orderFlagsRequestData)
 */
final class Flags implements Api, Put
{
    use SendsApiPutRequest;

    private const URI = '/Orders/{orderID}/Flags';

    public int $orderID = 0;
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'PUT' => OrderFlagsRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [];
}
