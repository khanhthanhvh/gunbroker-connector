<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\ShippingProfileSearch as ShippingProfileSearchRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\ShippingProfileSearch as ShippingProfileSearchResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ShippingProfileSearch
 * @implements Get<ShippingProfileSearchRequestData, ShippingProfileSearchResponseData>
 * @method ShippingProfileSearchResponseData get(ShippingProfileSearchRequestData $shippingProfileSearchRequestData)
 */
final class ShippingProfileSearch implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/ShippingProfileSearch';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ShippingProfileSearchRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ShippingProfileSearchResponseData::class
    ];
}
