<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\OrdersSold as OrdersSoldRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\OrdersSold as OrdersSoldResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/OrdersSold
 * @implements Get<OrdersSoldRequestData, OrdersSoldResponseData>
 * @method OrdersSoldResponseData get(OrdersSoldRequestData $ordersSoldRequestData)
 */
final class OrdersSold implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/OrdersSold';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => OrdersSoldRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => OrdersSoldResponseData::class
    ];
}
