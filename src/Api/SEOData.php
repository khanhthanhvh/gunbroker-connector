<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\SEOData as SEODataRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\SEOData as SEODataResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/SEOData
 * @implements Get<SEODataRequestData, SEODataResponseData>
 * @method SEODataResponseData get(SEODataRequestData $seoDataRequestData)
 */
final class SEOData implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/SEOData';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => SEODataRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => SEODataResponseData::class
    ];
}
