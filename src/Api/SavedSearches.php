<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\SaveSearch as SaveSearchRequestData;
use Wagento\GunBrokerApi\ApiObjects\Input\SavedSearches as SavedSearchesRequestData;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\SavedSearches as SavedSearchesResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\HttpMethod\Post;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;
use Wagento\GunBrokerApi\Traits\SendsApiPostRequest;

/**
 * @see https://api.gunbroker.com/User/Help/SavedSearchesGet
 * @implements Get<SavedSearchesRequestData, SavedSearchesResponseData>
 * @implements Post<SaveSearchRequestData, MessageResponse>
 * @method SavedSearchesResponseData get(SavedSearchesRequestData $savedSearchesRequestData)
 * @method MessageResponse post(SaveSearchRequestData $savedSearchRequestData)
 */
final class SavedSearches implements Api, Get, Post
{
    use SendsApiGetRequest;
    use SendsApiPostRequest;

    private const URI = '/SavedSearches';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => SavedSearchesRequestData::class,
        'POST' => SaveSearchRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => SavedSearchesResponseData::class
    ];
}
