<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Http\Discovery\Psr17FactoryDiscovery;
use Http\Message\MultipartStream\MultipartStreamBuilder;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\Item as ItemRequestData;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemSearch as ItemSearchRequestData;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemSearch as ItemSearchResponseData;
use Wagento\GunBrokerApi\Exception\ConnectorException;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\HttpMethod\Post;
use Wagento\GunBrokerApi\Traits\ProcessesApiRequest;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

use function base64_decode;

/**
 * @implements Get<ItemSearchRequestData, ItemSearchResponseData>
 * @implements Post<ItemRequestData, MessageResponse>
 * @method ItemSearchResponseData get(ItemSearchRequestData $requestData)
 */
final class Items implements Api, Get, Post
{
    use ProcessesApiRequest;
    use SendsApiGetRequest;

    private const URI = '/Items';

    /**
     * @var array<int, array{name: string, filename: string, data:string}>
     */
    public array $pictures = [];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ItemSearchRequestData::class,
        'POST' => ItemRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ItemSearchResponseData::class
    ];
    private StreamFactoryInterface $streamFactory;
    private RequestFactoryInterface $requestFactory;
    private MultipartStreamBuilder $multipartStreamBuilder;

    public function __construct(
        ?StreamFactoryInterface $streamFactory = null,
        ?RequestFactoryInterface $requestFactory = null,
        ?MultipartStreamBuilder $multipartStreamBuilder = null
    ) {
        $this->streamFactory = $streamFactory ?? Psr17FactoryDiscovery::findStreamFactory();
        $this->requestFactory = $requestFactory ?? Psr17FactoryDiscovery::findRequestFactory();
        $this->multipartStreamBuilder = $multipartStreamBuilder ?? new MultipartStreamBuilder($this->streamFactory);
    }

    /**
     * @param ItemRequestData $requestData
     * @return MessageResponse
     * @throws ConnectorException
     */
    public function post(DataTransferObject $requestData): DataTransferObject
    {
        $this->validateInputObject($requestData, 'POST');

        $requestBody = $this->processRequest($requestData->all());

        $this->multipartStreamBuilder->addResource('data', $requestBody);

        foreach ($this->pictures as $picture) {
            $this->multipartStreamBuilder->addResource(
                $picture['name'],
                base64_decode($picture['data'], true) ?: '',
                ['filename' => $picture['filename']]
            );
        }

        $request = $this->requestFactory->createRequest('POST', self::URI)
            ->withHeader(
                'Content-Type',
                "multipart/form-data; boundary=\"{$this->multipartStreamBuilder->getBoundary()}\""
            )
            ->withBody($this->multipartStreamBuilder->build());
        $responseData = $this->processResponse($this->httpClient->sendRequest($request));

        $this->multipartStreamBuilder->reset();

        return new MessageResponse($responseData);
    }
}
