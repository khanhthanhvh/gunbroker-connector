<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\Shipments as ShipmentsRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\Shipments as ShipmentsResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ShipmentsGet
 * @implements Get<ShipmentsRequestData, ShipmentsResponseData>
 * @method ShipmentsResponseData get(ShipmentsRequestData $shipmentsRequestData)
 */
final class Shipments implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/Shipments';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ShipmentsRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ShipmentsResponseData::class
    ];
}
