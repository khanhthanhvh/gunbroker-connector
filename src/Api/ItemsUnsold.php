<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsUnsold as ItemsUnsoldRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsUnsold as ItemsUnsoldResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ItemsUnsold
 * @implements Get<ItemsUnsoldRequestData, ItemsUnsoldResponseData>
 * @method ItemsUnsoldResponseData get(ItemsUnsoldRequestData $itemsUnsoldRequestData)
 */
final class ItemsUnsold implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/ItemsUnsold';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ItemsUnsoldRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ItemsUnsoldResponseData::class
    ];
}
