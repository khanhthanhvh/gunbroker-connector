<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api\ShippingProfile;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Input\ShippingProfileByValue as ShippingProfileByValueRequestData;
use Wagento\GunBrokerApi\ApiObjects\Output\ShippingProfileByValue as ShippingProfileByValueResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/ShippingProfileByValueTypeGet
 * @implements Get<ShippingProfileByValueRequestData, ShippingProfileByValueResponseData>
 * @method ShippingProfileByValueResponseData get(ShippingProfileByValueRequestData $shippingProfileByValueRequestData)
 */
final class Value implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/ShippingProfile';

    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [
        'GET' => ShippingProfileByValueRequestData::class
    ];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => ShippingProfileByValueResponseData::class
    ];
}
