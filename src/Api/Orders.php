<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\Api;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Api;
use Wagento\GunBrokerApi\ApiObjects\Output\Order as OrderResponseData;
use Wagento\GunBrokerApi\HttpMethod\Get;
use Wagento\GunBrokerApi\Traits\SendsApiGetRequest;

/**
 * @see https://api.gunbroker.com/User/Help/OrdersGet
 * @implements Get<DataTransferObject, OrderResponseData>
 * @method OrderResponseData get()
 */
final class Orders implements Api, Get
{
    use SendsApiGetRequest;

    private const URI = '/Orders/{orderID}';

    public int $orderID = 0;
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $inputObjects = [];
    /**
     * @var array<string, class-string<DataTransferObject>>
     */
    private array $outputObjects = [
        'GET' => OrderResponseData::class
    ];
}
