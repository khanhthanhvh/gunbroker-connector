<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\HttpMethod;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Exception\ConnectorException;

/**
 * @template TPostInput of DataTransferObject
 * @template TPostOutput of DataTransferObject
 */
interface Post
{
    /**
     * @param TPostInput $requestData
     * @return TPostOutput
     * @throws ConnectorException
     */
    public function post(DataTransferObject $requestData): DataTransferObject;
}
