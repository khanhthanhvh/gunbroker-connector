<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\HttpMethod;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Exception\ConnectorException;

/**
 * @template TGetInput of DataTransferObject
 * @template TGetOutput
 */
interface Get
{
    /**
     * @param TGetInput|null $requestData
     * @return TGetOutput
     * @throws ConnectorException
     *
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingNativeTypeHint
     */
    public function get(?DataTransferObject $requestData = null);
}
