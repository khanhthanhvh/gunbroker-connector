<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\HttpMethod;

use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\Exception\ConnectorException;

interface Delete
{
    /**
     * @throws ConnectorException
     */
    public function delete(): MessageResponse;
}
