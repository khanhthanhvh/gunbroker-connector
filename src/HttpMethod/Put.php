<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\HttpMethod;

use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\Exception\ConnectorException;

/**
 * @template TPutInput of DataTransferObject
 * @template TPutOutput of DataTransferObject
 */
interface Put
{
    /**
     * @param TPutInput $requestData
     * @return TPutOutput
     * @throws ConnectorException
     */
    public function put(DataTransferObject $requestData): DataTransferObject;
}
