<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\HttpClient\Authentication;

use Http\Message\Authentication;
use Psr\Http\Message\RequestInterface;

final class AccessToken implements Authentication
{
    private string $accessToken;

    public function __construct(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public function authenticate(RequestInterface $request): RequestInterface
    {
        return $request->withHeader('X-AccessToken', $this->accessToken);
    }
}
