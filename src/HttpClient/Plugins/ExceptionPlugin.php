<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\HttpClient\Plugins;

use Http\Client\Common\Plugin;
use Http\Promise\Promise;
use JsonException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\Exception\BadRequestException;
use Wagento\GunBrokerApi\Exception\CouldNotAddItemException;
use Wagento\GunBrokerApi\Exception\CouldNotDeleteItemException;
use Wagento\GunBrokerApi\Exception\ExpiredAccessTokenException;
use Wagento\GunBrokerApi\Exception\ForbiddenRequestException;
use Wagento\GunBrokerApi\Exception\GenericApiException;
use Wagento\GunBrokerApi\Exception\InvalidUserCredentialsException;
use Wagento\GunBrokerApi\Exception\InvalidUsernameOrPasswordException;
use Wagento\GunBrokerApi\Exception\MissingAccessTokenException;
use Wagento\GunBrokerApi\Exception\NotFoundException;
use Wagento\GunBrokerApi\Exception\ServerErrorException;
use Wagento\GunBrokerApi\Exception\ValidationException;

use function json_decode;
use function str_starts_with;

use const JSON_THROW_ON_ERROR;

final class ExceptionPlugin implements Plugin
{
    public function handleRequest(RequestInterface $request, callable $next, callable $first): Promise
    {
        $promise = $next($request);

        return $promise->then(
            fn(ResponseInterface $response) => $this->transformResponseToException($request, $response)
        );
    }

    /**
     * @throws \Wagento\GunBrokerApi\Exception\ApiException
     */
    private function transformResponseToException(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $statusCode = $response->getStatusCode();

        if ($statusCode >= 200 && $statusCode <= 399) {
            return $response;
        }

        $exception = null;
        $requestPath = $request->getUri()->getPath();

        if ($statusCode === 400) {
            if ($requestPath === '/Users/AccessToken' && $request->getMethod() === 'DELETE') {
                $exception = new MissingAccessTokenException();
            } else {
                $exception = new BadRequestException();
            }
        }

        if ($statusCode === 401) {
            if ($requestPath === '/Users/AccessToken' && $request->getMethod() === 'POST') {
                $exception = new InvalidUsernameOrPasswordException();
            } else {
                $exception = new ExpiredAccessTokenException();
            }
        }

        if ($statusCode === 403) {
            if ($requestPath === '/BillingInformation' && $request->getMethod() === 'GET') {
                $exception = new InvalidUserCredentialsException();
            } else {
                $exception = new ForbiddenRequestException();
            }
        }

        if ($statusCode === 404) {
            $exception = new NotFoundException();
        }

        if ($statusCode === 409) {
            if ($requestPath === '/Feedback' && $request->getMethod() === 'POST') {
                $exception = new ValidationException();
            }

            if (str_starts_with($requestPath, '/ItemsWatched')) {
                if ($request->getMethod() === 'POST') {
                    $exception = new CouldNotAddItemException();
                } elseif ($request->getMethod() === 'DELETE') {
                    $exception = new CouldNotDeleteItemException();
                }
            }
        }

        if ($statusCode === 500) {
            $exception = new ServerErrorException();
        }

        if ($exception === null) {
            $exception = new GenericApiException();
        }

        $responseData = $this->processResponse($response);

        if ($responseData !== null) {
            $exception->setMessageResponse(new MessageResponse($responseData));
        }

        throw $exception;
    }

    /**
     * @return mixed[]|null
     */
    private function processResponse(ResponseInterface $response): ?array
    {
        try {
            $responseData = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            return null;
        } finally {
            $response->getBody()->rewind();
        }

        return $responseData;
    }
}
