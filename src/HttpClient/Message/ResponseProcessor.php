<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\HttpClient\Message;

use JsonException;
use Psr\Http\Message\ResponseInterface;
use Wagento\GunBrokerApi\Exception\InvalidApiResponseException;

use function json_decode;
use function str_contains;

use const JSON_THROW_ON_ERROR;

final class ResponseProcessor implements ResponseProcessorInterface
{
    /**
     * @return mixed[]
     */
    public function processResponse(ResponseInterface $response): array
    {
        $responseBody = $response->getBody()->__toString();

        if (!str_contains($response->getHeaderLine('Content-Type'), 'application/json')) {
            throw InvalidApiResponseException::withResponseBody(
                $responseBody,
                'Invalid response data type returned from GunBroker.com API.'
            );
        }

        try {
            $processedResponse = json_decode($responseBody, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw InvalidApiResponseException::withResponseBody(
                $responseBody,
                'An error occurred while processing the response from the GunBroker.com API.',
                0,
                $e
            );
        } finally {
            $response->getBody()->rewind();
        }

        return $processedResponse;
    }
}
