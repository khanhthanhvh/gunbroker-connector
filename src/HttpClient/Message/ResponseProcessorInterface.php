<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\HttpClient\Message;

use Psr\Http\Message\ResponseInterface;

interface ResponseProcessorInterface
{
    /**
     * @return mixed[]
     */
    public function processResponse(ResponseInterface $response): array;
}
