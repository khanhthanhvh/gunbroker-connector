<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\HttpClient\Message;

use JsonException;
use Wagento\GunBrokerApi\Exception\InvalidApiRequestException;
use Wagento\GunBrokerApi\Utils\RecursiveNullFilter;

use function json_encode;

use const JSON_THROW_ON_ERROR;

final class RequestProcessor implements RequestProcessorInterface
{
    /**
     * @param mixed[] $requestData
     */
    public function processRequest(array $requestData, bool $allowNullValues = false): string
    {
        if (!$allowNullValues) {
            $requestData = RecursiveNullFilter::filter($requestData);
        }

        try {
            $processedRequest = json_encode($requestData, JSON_THROW_ON_ERROR, 512);
        } catch (JsonException $e) {
            throw InvalidApiRequestException::withRequestData(
                $requestData,
                'An error occurred while processing the request data to send to the GunBroker.com API.',
                0,
                $e
            );
        }

        return $processedRequest;
    }
}
