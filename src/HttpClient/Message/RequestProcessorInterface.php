<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\HttpClient\Message;

interface RequestProcessorInterface
{
    /**
     * @param mixed[] $requestData
     */
    public function processRequest(array $requestData, bool $allowNullValues = false): string;
}
