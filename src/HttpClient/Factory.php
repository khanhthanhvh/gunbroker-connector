<?php

declare(strict_types=1);

namespace Wagento\GunBrokerApi\HttpClient;

use Http\Client\Common\HttpMethodsClient;
use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin;
use Http\Client\Common\Plugin\HeaderDefaultsPlugin;
use Http\Client\Common\PluginClientFactory;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\Psr17FactoryDiscovery;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Wagento\GunBrokerApi\HttpClient\Plugins\ExceptionPlugin;

use function array_filter;
use function array_key_exists;
use function array_walk;

final class Factory
{
    private ClientInterface $httpClient;
    private RequestFactoryInterface $requestFactory;
    private StreamFactoryInterface $streamFactory;
    private PluginClientFactory $pluginClientFactory;
    /**
     * @var \Http\Client\Common\Plugin[]
     */
    private array $httpClientPlugins = [];
    /**
     * @var array<string, string> List of headers with the header name as the key and value as value
     */
    private array $httpHeaders = [];

    public function __construct(
        ?ClientInterface $httpClient = null,
        ?RequestFactoryInterface $requestFactory = null,
        ?StreamFactoryInterface $streamFactory = null,
        ?PluginClientFactory $pluginClientFactory = null
    ) {
        $this->httpClient = $httpClient ?? HttpClientDiscovery::find();
        $this->requestFactory = $requestFactory ?? Psr17FactoryDiscovery::findRequestFactory();
        $this->streamFactory = $streamFactory ?? Psr17FactoryDiscovery::findStreamFactory();
        $this->pluginClientFactory = $pluginClientFactory ?? new PluginClientFactory();

        $this->addDefaultPlugins();
    }

    public function addPlugin(Plugin $plugin): self
    {
        $this->httpClientPlugins[] = $plugin;

        return $this;
    }

    /**
     * @param \Http\Client\Common\Plugin[] $plugins
     */
    public function addPlugins(array $plugins): self
    {
        array_walk($plugins, fn(Plugin $plugin) => $this->addPlugin($plugin));

        return $this;
    }

    public function removePlugin(string $pluginClass): self
    {
        $this->httpClientPlugins = array_filter(
            $this->httpClientPlugins,
            fn(Plugin $plugin) => !$plugin instanceof $pluginClass
        );

        return $this;
    }

    /**
     * @param string[] $pluginClasses
     */
    public function removePlugins(array $pluginClasses): self
    {
        array_walk($pluginClasses, fn(string $pluginClass) => $this->removePlugin($pluginClass));

        return $this;
    }

    public function resetPlugins(): self
    {
        $this->httpClientPlugins = [];

        return $this;
    }

    public function hasPlugin(string $pluginClass): bool
    {
        foreach ($this->httpClientPlugins as $httpClientPlugin) {
            if (!$httpClientPlugin instanceof $pluginClass) {
                continue;
            }

            return true;
        }

        return false;
    }

    /**
     * @return \Http\Client\Common\Plugin[]
     */
    public function getPlugins(): array
    {
        return $this->httpClientPlugins;
    }

    public function addHttpHeader(string $name, string $value): self
    {
        $this->httpHeaders[$name] = $value;

        return $this;
    }

    /**
     * @param array<string, string> $httpHeaders List of headers with the header name as the key and value as value
     */
    public function addHttpHeaders(array $httpHeaders): self
    {
        array_walk($httpHeaders, fn(string $value, string $name) => $this->addHttpHeader($name, $value));

        return $this;
    }

    public function removeHttpHeader(string $name): self
    {
        if (!$this->hasHttpHeader($name)) {
            return $this;
        }

        unset($this->httpHeaders[$name]);

        return $this;
    }

    /**
     * @param string[] $headerNames
     */
    public function removeHttpHeaders(array $headerNames): self
    {
        array_walk($headerNames, fn(string $name) => $this->removeHttpHeader($name));

        return $this;
    }

    public function resetHttpHeaders(): self
    {
        $this->httpHeaders = [];

        return $this;
    }

    public function hasHttpHeader(string $headerName): bool
    {
        return array_key_exists($headerName, $this->httpHeaders);
    }

    /**
     * @return array<string, string>
     */
    public function getHttpHeaders(): array
    {
        return $this->httpHeaders;
    }

    public function create(): HttpMethodsClientInterface
    {
        $this->addPlugin(new Plugin\HeaderSetPlugin($this->httpHeaders));

        $pluginClient = $this->pluginClientFactory->createClient($this->httpClient, $this->httpClientPlugins);

        return new HttpMethodsClient($pluginClient, $this->requestFactory, $this->streamFactory);
    }

    private function addDefaultPlugins(): void
    {
        $this->httpClientPlugins[] = new ExceptionPlugin();
        $this->httpClientPlugins[] = new HeaderDefaultsPlugin([
            'User-Agent' => 'Wagento GunBroker API Connector (https://www.wagento.com/)',
            'Content-Type' => 'application/json',
        ]);
    }
}
